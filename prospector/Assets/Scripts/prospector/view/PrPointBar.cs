﻿using UnityEngine;
using UnityEngine.UI;

namespace prospector.view
{

    public class PrPointBar : MonoBehaviour
    {
        public Color emptyColor;
        public Color fillColor;
        public Text label;
        public Image[] barPoints;

        private int mLastValue;
        private int mLastHalfValue;

        public void SetValue(int value, int halfValue = -1)
        {
            if (halfValue == -1)
            {
                halfValue = value;
            }
            if (mLastValue == value && mLastHalfValue == halfValue)
            {
                return;
            }
            mLastValue = value;
            mLastHalfValue = halfValue;
            UnityEngine.Assertions.Assert.IsTrue(value >= 0 && value <= barPoints.Length);
            Color halfColor = Color.Lerp(emptyColor, fillColor, 0.5f);
            for (int i = 0; i < barPoints.Length; ++i)
            {
                Color color = fillColor;
                if (i >= mLastValue)
                {
                    color = i >= mLastHalfValue ? emptyColor : halfColor;
                }
                barPoints[i].color = color;
            }
        }
    }

}
