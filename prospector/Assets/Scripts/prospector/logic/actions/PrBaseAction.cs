﻿using System.Text;

namespace prospector.logic.actions
{

    public abstract class PrBaseAction
    {

        private readonly int mPlayerID;

        public PrBaseAction(int playerID)
        {
            mPlayerID = playerID;
        }

        public int playerID
        {
            get { return mPlayerID; }
        }

        public bool Do(PrGameField gameField, PrGameEvents events, bool logEvents)
        {
            PrMessageBuilder mb = new PrMessageBuilder(logEvents);
            AppendCommandInfo(mb);

            bool success = false;

            if (IsCanDo(gameField, mb))
            {
                DoAction(gameField, events);
                success = true;
            }
            else if (IsCanDoFallback(gameField, mb))
            {
                DoFallbackAction(gameField, events);
                success = true;
            }
            PrGameLog.AddLogForPlayer(mPlayerID, mb.GetAllMessage());
            return success;
        }

        protected abstract bool IsCanDo(PrGameField gameField, PrMessageBuilder mb);
        protected abstract bool IsCanDoFallback(PrGameField gameField, PrMessageBuilder mb);
        protected abstract void DoAction(PrGameField gameField, PrGameEvents events);
        protected abstract void DoFallbackAction(PrGameField gameField, PrGameEvents events);
        protected abstract void AppendCommandInfo(PrMessageBuilder mb);
    }

}
