﻿
namespace prospector.logic.actions
{

    public class PrBaseImproveStatAction : PrBaseAction
    {

        public PrBaseImproveStatAction(int playerID) :
            base(playerID)
        {
        }

        protected override void AppendCommandInfo(PrMessageBuilder mb)
        {
            mb.AppendLine("[ImproveMinSkill]");
        }

        protected override bool IsCanDo(PrGameField gameField, PrMessageBuilder mb)
        {
            return false;
        }

        protected override bool IsCanDoFallback(PrGameField gameField, PrMessageBuilder mb)
        {
            utils.Assert.IsTrue(IsValidPlayerID(gameField));
            if (!gameField.GetPlayerByID(playerID).stats.CanImproveAnySkill())
            {
                mb.AppendLine("Not enough stat points");
                return false;
            }
            return true;
        }

        protected override void DoAction(PrGameField gameField, PrGameEvents events)
        {
            utils.Assert.IsTrue(false);
        }

        protected override void DoFallbackAction(PrGameField gameField, PrGameEvents events)
        {
            gameField.GetPlayerByID(playerID).stats.ImproveMinSkill();
        }

        protected bool IsValidPlayerID(PrGameField gameField)
        {
            return gameField.GetPlayerByID(playerID) != null;
        }
    }

}

