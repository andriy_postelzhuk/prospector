﻿using System.Collections.Generic;
using prospector.logic.fieldObjects;

namespace prospector.logic
{

    public class PrCell
    {
        private Dictionary<EPrFOType, PrFieldObject> mFieldObjects;

        public PrCell()
        {
            mFieldObjects = new Dictionary<EPrFOType, PrFieldObject>();
        }

        public IDictionary<EPrFOType, PrFieldObject> allObjects
        {
            get { return mFieldObjects; }
        }

        public void AddFO(PrFieldObject fObj)
        {
            mFieldObjects.Add(fObj.typeID, fObj);
        }

        public void RemoveFO(PrFieldObject fObj)
        {
            utils.Assert.IsTrue(mFieldObjects[fObj.typeID] == fObj);
            mFieldObjects.Remove(fObj.typeID);
        }

        public void RemoveObjectOfType(EPrFOType typeID)
        {
            bool removed = mFieldObjects.Remove(typeID);
            utils.Assert.IsTrue(removed);
        }

        public bool HasObjectOfType(EPrFOType typeID)
        {
            return mFieldObjects.ContainsKey(typeID);
        }

        public PrFieldObject GetObjectOfType(EPrFOType typeID)
        {
            PrFieldObject obj = null;
            mFieldObjects.TryGetValue(typeID, out obj);
            return obj;
        }

        public bool hasAnyObjects
        {
            get { return mFieldObjects.Count > 0; }
        }
    }

}
