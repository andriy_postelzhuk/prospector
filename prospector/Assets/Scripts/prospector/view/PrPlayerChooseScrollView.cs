﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;

using prospector.logic.utils;
using prospector.view.utils;

namespace prospector.view
{

    public class PrPlayerChooseScrollView : MonoBehaviour
    {
        public ToggleGroup toggleGroup;
        public Toggle togglePrefab;
        public ScrollRect scrollRect;

        private Dictionary<string, Toggle> mAllToggles;

        void Awake()
        {
            mAllToggles = new Dictionary<string, Toggle>();
        }

        public event System.Action<PrPlayerChooseScrollView> NewPlayerChosenEvent;

        public string chosenPlayerName
        {
            get
            {
                string playerName = null;
                foreach (var pair in mAllToggles)
                {
                    if (pair.Value.isOn)
                    {
                        playerName = pair.Key;
                        break;
                    }
                }
                return playerName;
            }
        }

        public void AddPlayer(string name)
        {
            Assert.IsTrue(!mAllToggles.ContainsKey(name));

            Toggle playerToggle = scrollRect.content.AddChild(togglePrefab);
            mAllToggles.Add(name, playerToggle);
            playerToggle.GetComponentInChildren<Text>().text = name;
            playerToggle.isOn = false;
            playerToggle.group = toggleGroup;
            playerToggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        public void RemovePlayer(string name)
        {
            Toggle toggle = mAllToggles[name];
            mAllToggles.Remove(name);
            Destroy(toggle.gameObject);
            NewPlayerChosenEvent.Call(this);
        }

        private void OnToggleValueChanged(bool value)
        {
            NewPlayerChosenEvent.Call(this);
        }
    }

}
