﻿using System.Collections.Generic;

using prospector.logic.actions;

namespace prospector.logic
{

    public class PrMainController
    {

        private PrGameField mGameField;
        private EPrGameState mState;
        private int[] mPlayerIDs;
        private int mActivePlayerIndex;
        private int mWinnerID;
        private PrGameEvents mGameEvents;
        private PrPlayerActivityGuard mActivityGuard;
        private Dictionary<int, int> mActionCounters;

        public void StartGame(PrStartGameParams startParams)
        {
            utils.Assert.IsTrue(mState == EPrGameState.Undefined);
            mState = EPrGameState.CHOOSING_STATS;
            mPlayerIDs = startParams.playerInternalIDs;
            mGameField = new PrGameField(startParams);
            mActivePlayerIndex = -1;
            mWinnerID = -1;
            mGameEvents = startParams.gameEvents;
            if (mGameEvents == null)
            {
                mGameEvents = new PrGameEvents(false);
            }
            mActivityGuard = new PrPlayerActivityGuard();
            mActionCounters = new Dictionary<int, int>();
            foreach (int id in mPlayerIDs)
            {
                mActionCounters.Add(id, 0);
            }
        }

        public PrGameField gameField
        {
            get { return mGameField; }
        }

        public EPrGameState curState
        {
            get { return mState; }
        }

        //valid only for battle phase
        public int activePlayerID
        {
            get { return mPlayerIDs[mActivePlayerIndex]; }
        }

        //valid only for finish phase
        public int winnerID
        {
            get { return mWinnerID; }
        }

        public int winnerActionCount
        {
            get { return mActionCounters[mWinnerID]; }
        }

        public void ChooseStatsTurn(PrBaseImproveStatAction[] statActions)
        {
            utils.Assert.IsTrue(mState == EPrGameState.CHOOSING_STATS);
            utils.Assert.IsTrue(statActions.Length == mPlayerIDs.Length);
            for (int i = 0; i < statActions.Length; ++i)
            {
                if (statActions[i] == null)
                {
                    fieldObjects.PrPlayerFO player = mGameField.GetPlayerByID(mPlayerIDs[i]);
                    if (player.stats.CanImproveAnySkill())
                    {
                        statActions[i] = new PrBaseImproveStatAction(mPlayerIDs[i]);
                        PrGameLog.AddLogForPlayer(mPlayerIDs[i], "Add default improve stat action");
                    }
                }
                if (statActions[i] != null)
                {
                    utils.Assert.IsTrue(statActions[i].playerID == mPlayerIDs[i]);
                    statActions[i].Do(mGameField, mGameEvents, mGameEvents != null);
                }
                else
                {
                    PrGameLog.AddLogForPlayer(mPlayerIDs[i], "Skip improve stat action");
                }
            }
            CheckStateSwitching();
        }

        //action can be null (skip action)
        public void BattleTurn(PrBattleAction action)
        {
            utils.Assert.IsTrue(mState == EPrGameState.BATTLE);
            utils.Assert.IsTrue(activePlayer.actionPoints > 0);
            int actionPoints = activePlayer.actionPoints;
            bool needToSleep = true;
            if (action != null)
            {
                utils.Assert.IsTrue(action.playerID == activePlayerID);
                if (action.Do(mGameField, mGameEvents, mGameEvents != null))
                {
                    needToSleep = false;
                }
            }
            if (needToSleep)
            {
                PrGameLog.AddLogForPlayer(activePlayerID, "Sleep");
                activePlayer.SleepAction();
            }
            mActionCounters[activePlayerID]++;
            utils.Assert.IsTrue(actionPoints == activePlayer.actionPoints + 1);
            CheckStateSwitching();
            if (mState != EPrGameState.BATTLE)
            {
                utils.Assert.IsTrue(mWinnerID > 0);
                return;
            }

            if (activePlayer.actionPoints > 0)
            {
                return;
            }

            UpdateWaitPlayerIndex();

            if (mActivePlayerIndex == 0)
            {
                mGameField.AllPlayersFinishTurn(mGameEvents);
                CheckStateSwitching();
            }
        }

        private void UpdateWaitPlayerIndex()
        {
            ++mActivePlayerIndex;
            if (mActivePlayerIndex >= mPlayerIDs.Length)
            {
                mActivePlayerIndex = 0;
            }
            activePlayer.OnStartTurn();
        }

        private fieldObjects.PrPlayerFO activePlayer
        {
            get { return mGameField.GetPlayerByID(activePlayerID); }
        }

        private void CheckStateSwitching()
        {
            if (mState == EPrGameState.CHOOSING_STATS)
            {
                if (mGameField.IsAllPlayersStatsChosen())
                {
                    mState = EPrGameState.BATTLE;
                    mGameField.StartBattle();
                    utils.Assert.IsTrue(mActivePlayerIndex == -1);
                    UpdateWaitPlayerIndex();
                    utils.Assert.IsTrue(mActivePlayerIndex == 0);
                }
            }
            else if (mState == EPrGameState.BATTLE)
            {
                for (int i = 0; i < mPlayerIDs.Length; ++i)
                {
                    fieldObjects.PrPlayerFO player = mGameField.GetPlayerByID(mPlayerIDs[i]);
                    if (player.gatheredCrystals > mGameField.halfCrystalsCount)
                    {
                        mWinnerID = mPlayerIDs[i];
                        break;
                    }
                    fieldObjects.PrPlayerFO enemyPlayer = mGameField.GetEnemyPlayerFor(mPlayerIDs[i]);
                    if (enemyPlayer.isDead)
                    {
                        mWinnerID = mPlayerIDs[i];
                        break;
                    }
                }
                if (mWinnerID < 0)
                {
                    mWinnerID = mActivityGuard.OnTickFinished(mGameField.players);
                }
                if (mWinnerID > 0)
                {
                    mState = EPrGameState.FINISHED;
                }
            }
        }
    }

}