﻿using UnityEngine;
using UnityEngine.UI;

using prospector.logic;
using prospector.logic.fieldObjects;
using prospector.logic.utils;
using prospector.view.utils;

using System.Collections.Generic;

namespace prospector.view
{

    public class PrGameFieldView : MonoBehaviour
    {

        public RectTransform rectTransform;
        public RectTransform frameRect;
        public Image cellPrefab;
        public Sprite cellSprite1;
        public Sprite cellSprite2;

        public PrFieldObjectView crystalPrefab;
        public PrPlayerView playerPrefab;
        public PrFieldObjectView basePrefab;

        private Transform[,] mCellsObjects;
        private Dictionary<int, PrFieldObjectView> mAllFieldObjects;
        private Dictionary<int, PrFieldObjectView> mObjectsToDelete;
        private Dictionary<int, PrPlayerView> mPlayerViews;

        public void Init(PrGameField gameField, IPrPlayer[] players)
        {
            Assert.IsTrue(mCellsObjects == null && mAllFieldObjects == null && mObjectsToDelete == null);
            mAllFieldObjects = new Dictionary<int, PrFieldObjectView>();
            mObjectsToDelete = new Dictionary<int, PrFieldObjectView>();
            mPlayerViews = new Dictionary<int, PrPlayerView>();
            PrCell[,] cells = gameField.cells;

            mCellsObjects = new Transform[cells.GetLength(0), cells.GetLength(1)];

            float maxWidth = rectTransform.rect.width / cells.GetLength(0);
            float maxHeight = rectTransform.rect.height / cells.GetLength(1);
            float cellRealSize = Mathf.Min(maxWidth, maxHeight);

            Assert.IsTrue(Mathf.Approximately(cellPrefab.rectTransform.sizeDelta.x, cellPrefab.rectTransform.sizeDelta.y));
            float cellSize = cellPrefab.rectTransform.sizeDelta.x;

            frameRect.sizeDelta = new Vector2(cells.GetLength(0) * cellSize, cells.GetLength(1) * cellSize);

            Vector2 startPosShift = rectTransform.sizeDelta;
            startPosShift.x -= cells.GetLength(0) * cellSize;
            startPosShift.y -= cells.GetLength(1) * cellSize;
            startPosShift.x *= 0.5f;
            startPosShift.y *= -0.5f;

            for (int x = 0; x < cells.GetLength(0); ++x)
            {
                for (int y = 0; y < cells.GetLength(1); ++y)
                {
                    Image newCell = this.AddChild(cellPrefab);
                    newCell.rectTransform.localScale = Vector3.one;
                    newCell.rectTransform.anchoredPosition = new Vector2(x * cellSize, -y * cellSize) + startPosShift;
                    if ((x + y) % 2 == 0)
                    {
                        newCell.sprite = cellSprite1;
                    }
                    else
                    {
                        newCell.sprite = cellSprite2;
                    }
                    mCellsObjects[x, y] = newCell.transform;
                    mCellsObjects[x, y].gameObject.name = string.Format("cell_{0}_{1}", x, y);
                }
            }
            float scale = cellRealSize / cellSize;
            rectTransform.localScale = new Vector3(scale, scale, 1.0f);

            foreach (IPrPlayer playerData in players)
            {
                PrPlayerFO playerFO = gameField.GetPlayerByID(playerData.id);
                PrPlayerView playerView = Instantiate(playerPrefab) as PrPlayerView;
                mAllFieldObjects.Add(playerFO.objID, playerView);
                mPlayerViews.Add(playerFO.internalID, playerView);
                playerView.Init(playerData.viewSeed);
            }
        }

        public void OnTickFinished(PrGameField gameField, PrGameEvents gameEvents)
        {
            Dictionary<int, PrFieldObjectView> temp = mObjectsToDelete;
            mObjectsToDelete = mAllFieldObjects;
            mAllFieldObjects = temp;
            Assert.IsTrue(mAllFieldObjects.Count == 0);

            PrCell[,] cells = gameField.cells;
            for (int x = 0; x < cells.GetLength(0); ++x)
            {
                for (int y = 0; y < cells.GetLength(1); ++y)
                {
                    PrCell cell = cells[x, y];
                    Transform cellImage = mCellsObjects[x, y];
                    foreach (var pair in cell.allObjects)
                    {
                        PrFieldObject fieldObject = pair.Value;
                        PrFieldObjectView foView;
                        if (mObjectsToDelete.TryGetValue(fieldObject.objID, out foView))
                        {
                            mObjectsToDelete.Remove(fieldObject.objID);
                        }
                        else
                        {
                            foView = CreateFieldObjectView(fieldObject);
                            if (fieldObject is PrPlayerFO)
                            {
                                mPlayerViews[fieldObject.internalID] = foView as PrPlayerView;
                            }
                        }
                        mAllFieldObjects.Add(fieldObject.objID, foView);
                        foView.SetCellParent(cellImage);
                    }
                    
                }
            }
            foreach (var pair in mObjectsToDelete)
            {
                Destroy(pair.Value.gameObject);
            }
            foreach (var pair in mPlayerViews)
            {
                PrPlayerFO player = gameField.GetPlayerByID(pair.Key);
                if (player.isDead)
                {
                    continue;
                }
                
                pair.Value.OnTickFinished(player);
            }
            while (gameEvents != null && gameEvents.hasEvents)
            {
                SPrGameEvent e = gameEvents.PullNextEvent();
                mPlayerViews[e.playerID].OnGameEvent(e);
            }
            mObjectsToDelete.Clear();
        }

        private PrFieldObjectView CreateFieldObjectView(PrFieldObject fieldObject)
        {
            PrFieldObjectView prefab = null;
            switch (fieldObject.typeID)
            {
                case EPrFOType.BASE:
                    prefab = basePrefab;
                break;
                case EPrFOType.CRYSTAL:
                    prefab = crystalPrefab;
                break;
            }
            return Instantiate(prefab) as PrFieldObjectView;
        }

    }

}
