﻿using System.Text;
using prospector.logic.actions;
using prospector.logic.fieldObjects.player;

namespace prospector.logic.test
{

    public static class TestTurns
    {
        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestTurns Run =====");
            //determinated seed (1)
            //| _pb_ | _c__ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _c__ | _pb_

            PrStartGameParams startParams = TestGameFieldGeneration.CreateSimpleStartParams(4, 4, 1, 1);
            startParams.playerImproveStatPoints = 2;
            PrMainController main = new PrMainController();
            main.StartGame(startParams);
            //TestGameFieldGeneration.PrintField(main.gameField.cells, sb);
            utils.Assert.IsTrue(main.curState == EPrGameState.CHOOSING_STATS);
            main.ChooseStatsTurn(new[]
            {
                new PrImproveStatAction(1, EPrStat.ATTACK),
                new PrImproveStatAction(2, EPrStat.ATTACK)
            });
            utils.Assert.IsTrue(main.curState == EPrGameState.BATTLE);

            PrBattleAction battleAction;

            battleAction = new PrMoveAction(1, EPrMoveDir.RIGHT);
            utils.Assert.IsTrue(main.activePlayerID == 1);
            main.BattleTurn(battleAction);

            utils.Assert.IsTrue(main.activePlayerID == 2);

            //| _pb_ | _pc_ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _c__ | _pb_

            battleAction = new PrMoveAction(2, EPrMoveDir.LEFT);
            main.BattleTurn(battleAction);


            //| _pb_ | _pc_ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _cp_ | __b_

            utils.Assert.IsTrue(main.gameField.CheckAllObjectsInCells());

            battleAction = new PrTakeCrystalAction(1);
            main.BattleTurn(battleAction);

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).gatheredCrystals == 0);

            //| _pb_ | _p__ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _cp_ | __b_

            battleAction = new PrMoveAction(2, EPrMoveDir.LEFT);
            main.BattleTurn(battleAction);


            //| _pb_ | _p__ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | _p__ | _c__ | __b_

            battleAction = new PrMoveAction(1, EPrMoveDir.LEFT);
            main.BattleTurn(battleAction);

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).gatheredCrystals == 1);

            battleAction = new PrMoveAction(2, EPrMoveDir.LEFT);
            main.BattleTurn(battleAction);


            //| _pb_ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| _p__ | ____ | _c__ | __b_

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).gatheredCrystals == 1);

            battleAction = new PrMoveAction(1, EPrMoveDir.DOWN);
            main.BattleTurn(battleAction);


            battleAction = new PrAttackAction(2);
            main.BattleTurn(battleAction);


            //| __b_ | ____ | ____ | ____
            //| _p__ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| _p__ | ____ | _c__ | __b_

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).healthPoints == 13);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).healthPoints == 15);

            battleAction = new PrMoveAction(1, EPrMoveDir.LEFT); //bad move
            main.BattleTurn(battleAction);


            battleAction = new PrMoveAction(2, EPrMoveDir.UP);
            main.BattleTurn(battleAction);


            //| _b__ | ____ | ____ | ____
            //| _p__ | ____ | ____ | ____
            //| _p__ | ____ | ____ | _c__
            //| ____ | ____ | ____ | __b_

            battleAction = new PrMoveAction(1, EPrMoveDir.LEFT); //bad move
            main.BattleTurn(battleAction);

            battleAction = new PrMoveAction(2, EPrMoveDir.LEFT); //bad move
            main.BattleTurn(battleAction);

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).healthPoints == 13);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).healthPoints == 15);

            battleAction = new PrMoveAction(1, EPrMoveDir.UP);
            main.BattleTurn(battleAction);


            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).healthPoints == 13);

            battleAction = new PrMoveAction(2, EPrMoveDir.UP);
            main.BattleTurn(battleAction);


            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).healthPoints == 15); //base healing
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).healthPoints == 15);

            //| _pb_ | ____ | ____ | ____
            //| _p__ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _c__ | __b_

            battleAction = new PrMoveAction(1, EPrMoveDir.UP); //bad move
            main.BattleTurn(battleAction);


            battleAction = new PrMoveAction(2, EPrMoveDir.UP); //bad move
            main.BattleTurn(battleAction);


            battleAction = new PrMoveAction(1, EPrMoveDir.RIGHT);
            main.BattleTurn(battleAction);


            battleAction = new PrAttackAction(2);
            main.BattleTurn(battleAction);

            //| _b__ | _p__ | ____ | ____
            //| _p__ | ____ | ____ | ____
            //| ____ | ____ | ____ | ____
            //| ____ | ____ | _c__ | __b_

            for (int i = 0; i < 7; ++i)
            {
                main.BattleTurn(null);
                main.BattleTurn(battleAction);
            }

            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).healthPoints == 0);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).isDead);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).healthPoints == 15);

            utils.Assert.IsTrue(main.curState == EPrGameState.FINISHED);
            utils.Assert.IsTrue(main.winnerID == 2);

            sb.AppendLine("===== TestTurns success =====");

        }
    }
}
