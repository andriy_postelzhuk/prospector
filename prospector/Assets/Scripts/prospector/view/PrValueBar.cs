﻿using UnityEngine;
using UnityEngine.UI;

namespace prospector.view
{

    public class PrValueBar : MonoBehaviour
    {
        public Image valueImg;
        public Image backValueImg;
        public Text valueLabel;
        public Text label;

        public void SetValue(int curValue, int maxValue)
        {
            Vector2 sizeDelta = backValueImg.rectTransform.sizeDelta;
            if (maxValue < 1 || curValue < 0)
            {
                sizeDelta.x = 0;
            }
            else
            {
                sizeDelta.x = sizeDelta.x * curValue / maxValue;
            }
            valueImg.rectTransform.sizeDelta = sizeDelta;
            valueLabel.text = curValue + "/" + maxValue;
        }
    }

}
