﻿using UnityEngine;
using UnityEngine.UI;
using prospector.logic;
using prospector.logic.bots;
using prospector.logic.test;

namespace prospector.view
{

    public class PrViewController : MonoBehaviour
    {

        private static float sLastSavedSpeedValue = 1.0f;

        public PrGameFieldView gameFieldView;
        public PrGameStatesWindow gameStatesWindow;
        public PrStatsView[] statsViews;
        public int framesPerTick;
        public int newStateDelay;
        public Button pauseButton;
        public Slider speedMultiplier;

        private PrEmulator mEmulator;
        private EPrGameState mLastGameState;
        private bool mTickInProgress;
        private int mFramesCountFromLastTick;
        private int mNewStateDelay;
        private PrGameEvents mGameEvents;

        void Start()
        {
            IPrPlayer[] players = PrGameFlow.players;
            for (int i = 0; i < players.Length; ++i)
            {
                statsViews[i].Init(players[i].id, players[i].name, players[i].avatarURL);
            }
            PrStartGameParams startParams = PrAllConsts.CreateStartParams();
            mGameEvents = new PrGameEvents(true);
            startParams.gameEvents = mGameEvents;
            mEmulator = new PrEmulator(players, startParams);
            mEmulator.OnTickFinished += OnTickFinished;
            mEmulator.OnGameFinished += OnGameFinished;
            mLastGameState = EPrGameState.Undefined;
            gameFieldView.Init(mEmulator.gameField, players);
            gameStatesWindow.Init();

            OnNewGameState(EPrGameState.CHOOSING_STATS);
            mNewStateDelay = 1;

            pauseButton.onClick.AddListener(OnClickPause);

            speedMultiplier.value = sLastSavedSpeedValue;
        }

        private void OnClickPause()
        {
            enabled = false;
            pauseButton.gameObject.SetActive(false);
            gameStatesWindow.Pause(OnGameResumed);
        }

        private void OnGameResumed()
        {
            enabled = true;
            pauseButton.gameObject.SetActive(true);
        }

        void Update()
        {
            if (mNewStateDelay > 0)
            {
                --mNewStateDelay;
                if (mNewStateDelay == 0)
                {
                    gameStatesWindow.OnNewGameState(mLastGameState);
                    foreach (PrStatsView statView in statsViews)
                    {
                        statView.OnNewGameState(mLastGameState);
                    }
                }
                return;
            }

            ++mFramesCountFromLastTick;
            if (mTickInProgress)
            {
                return;
            }
            sLastSavedSpeedValue = speedMultiplier.value;
            if (framesPerTick / speedMultiplier.value <= mFramesCountFromLastTick)
            {
                mFramesCountFromLastTick = 0;
                mTickInProgress = true;
                mEmulator.StartTick();
            }
        }

        private void OnNewGameState(EPrGameState newGameState)
        {
            mLastGameState = newGameState;
            mNewStateDelay = newStateDelay;
        }

        private void OnTickFinished()
        {
            mTickInProgress = false;
            gameFieldView.OnTickFinished(mEmulator.gameField, mGameEvents);
            foreach (PrStatsView statView in statsViews)
            {
                statView.OnTickFinished(mEmulator.gameField, mLastGameState);
            }
            if (mEmulator.gameState != mLastGameState)
            {
                OnNewGameState(mEmulator.gameState);
            }
        }

        private void OnGameFinished()
        {
            pauseButton.gameObject.SetActive(false);
            gameStatesWindow.OnGameFinished(mEmulator.winnerPlayerName, mEmulator.winnerActionCount);
        }
    }

}