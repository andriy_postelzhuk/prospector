﻿using prospector.logic.utils;

namespace prospector.logic.data
{

    public class PrPlayerData : PrFieldObjectData
    {
        public PrPlayerStatsData stats;
        public int healthPoints;
        public int actionsPoints;
        public int gatheredCrystals;
        public int crystalsCount;

        public override bool Equals(object obj)
        {
            PrPlayerData playerData = obj as PrPlayerData;
            if (playerData == null)
            {
                return false;
            }
            return base.Equals(playerData) &&
                stats.Equals(playerData.stats) &&
                healthPoints == playerData.healthPoints &&
                actionsPoints == playerData.actionsPoints &&
                gatheredCrystals == playerData.gatheredCrystals &&
                crystalsCount == playerData.crystalsCount;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
