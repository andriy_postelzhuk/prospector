﻿using prospector.logic.fieldObjects.player;

namespace prospector.view
{

    public static class PrLocalization
    {
        private const string STAT_AGILITY = "Agility";
        private const string STAT_ATTACK = "Attack";
        private const string STAT_HEALTH = "Health";

        public const string VALUE_STAT_POINTS = "Stat points:";
        public const string VALUE_ACTION_POINTS = "AP:";
        public const string VALUE_HP = "HP:";
        public const string VALUE_CRYSTALS = "Crystals:";

        public const string STATE_CHOOSING_STATS = "Choosing stats...";
        public const string STATE_FINISHING = "[PLAYER_NAME]\nIS THE WINNER!!!\n Actions count: [ACTIONS_COUNT]";

        public const string BUTTON_LABEL_START_GAME = "Start game";
        public const string BUTTON_LABEL_NEW_GAME = "New game";
        public const string BUTTON_LABEL_RESTART = "Restart";
        public const string BUTTON_LABEL_RESUME = "Resume";
        public const string BUTTON_LABEL_ADD_PLAYER = "<ADD>";

        public static string GetStatLabel(EPrStat stat)
        {
            switch (stat)
            {
                case EPrStat.AGILITY:
                    return STAT_AGILITY;
                case EPrStat.ATTACK:
                    return STAT_ATTACK;
                case EPrStat.HEALTH:
                    return STAT_HEALTH;
            }
            return "";
        }
    }

}
