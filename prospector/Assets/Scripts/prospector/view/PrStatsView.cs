﻿using UnityEngine;

using prospector.logic;
using prospector.logic.fieldObjects;
using prospector.logic.fieldObjects.player;
using prospector.view.utils;

using System.Collections.Generic;

namespace prospector.view
{

    public class PrStatsView : MonoBehaviour
    {
        public PrPointBar pointPrefab;
        public PrValueBar valuePrefab;
        public PrTextBar textPrefab;
        public PrAvatar avatarPrefab;
        public PrLabelBar labelPrefab;
        public PrLogText logPrefab;

        private Dictionary<EPrStat, PrPointBar> mPointBars;
        private PrTextBar mStatPointsBar;
        private PrPointBar mActionPoints;
        private PrValueBar mHealthBar;
        private PrTextBar mCrystalBar;
        private PrLabelBar mNameBar;
        private PrAvatar mAvatar;
        private PrLogText mLog;
        private int mPlayerID;

        public void Init(int playerID, string name, string avatarUrl)
        {
            mPlayerID = playerID;

            mStatPointsBar = this.AddChild(textPrefab);
            mStatPointsBar.label.text = PrLocalization.VALUE_STAT_POINTS;
            mStatPointsBar.valueLabel.text = PrAllConsts.STAT_POINTS.ToString();

            mPointBars = new Dictionary<EPrStat, PrPointBar>();
            for (int i = 0; i < (int)EPrStat.COUNT; ++i)
            {
                CreatePointBar((EPrStat)i);
            }

            mActionPoints = this.AddChild(pointPrefab);
            mActionPoints.label.text = PrLocalization.VALUE_ACTION_POINTS;
            mActionPoints.fillColor = GetBarStatColor(EPrStat.AGILITY);
            mActionPoints.SetValue(0);

            mHealthBar = this.AddChild(valuePrefab);
            mHealthBar.label.text = PrLocalization.VALUE_HP;
            mHealthBar.valueImg.color = GetBarStatColor(EPrStat.HEALTH);
            mHealthBar.backValueImg.color = GetBarStatColor(EPrStat.ATTACK);
            mHealthBar.SetValue(0, 0);

            mCrystalBar = this.AddChild(textPrefab);
            mCrystalBar.label.text = PrLocalization.VALUE_CRYSTALS;
            mCrystalBar.valueLabel.text = "";

            mNameBar = this.AddChild(labelPrefab);
            mNameBar.label.text = name;

            mAvatar = this.AddChild(avatarPrefab);
            mAvatar.Load(avatarUrl);

            mLog = this.AddChild(logPrefab);
            mLog.label.text = "";
        }

        public void OnNewGameState(EPrGameState gameState)
        {
            bool isChoosingStats = gameState == EPrGameState.CHOOSING_STATS;
            mStatPointsBar.gameObject.SetActive(isChoosingStats);
            mPointBars[EPrStat.AGILITY].gameObject.SetActive(isChoosingStats);
            mPointBars[EPrStat.HEALTH].gameObject.SetActive(isChoosingStats);
            mActionPoints.gameObject.SetActive(!isChoosingStats);
            mHealthBar.gameObject.SetActive(!isChoosingStats);
            mCrystalBar.gameObject.SetActive(!isChoosingStats);
        }

        public void OnTickFinished(PrGameField gameField, EPrGameState gameState)
        {
            PrPlayerFO player = gameField.GetPlayerByID(mPlayerID);
            if (gameState == EPrGameState.CHOOSING_STATS)
            {
                mStatPointsBar.valueLabel.text = player.stats.statsPoints.ToString();
                foreach (var pair in mPointBars)
                {
                    pair.Value.SetValue(player.stats.GetStatValue(pair.Key));
                }
            }
            else
            {
                mHealthBar.SetValue(player.healthPoints, player.maxHealthPoints);
                mCrystalBar.valueLabel.text = string.Format("{0} (+{1})", player.gatheredCrystals, player.crystalsCount);
                mActionPoints.SetValue(player.actionPoints, player.stats.agility);
            }
            mLog.OnTickFinished(mPlayerID);
        }

        private void CreatePointBar(EPrStat stat)
        {
            PrPointBar pointBar = this.AddChild(pointPrefab);
            pointBar.label.text = PrLocalization.GetStatLabel(stat);
            pointBar.fillColor = GetBarStatColor(stat);
            mPointBars.Add(stat, pointBar);
            pointBar.SetValue(PrPlayerStats.START_STAT_VALUE);
        }

        private static Color GetBarStatColor(EPrStat stat)
        {
            switch (stat)
            {
                case EPrStat.AGILITY:
                    return Color.blue;
                case EPrStat.ATTACK:
                    return Color.red;
                case EPrStat.HEALTH:
                    return Color.green;
            }
            return Color.black;
        }
    }

}
