﻿using System.Collections.Generic;
using prospector.logic.fieldObjects;

namespace prospector.logic
{

    public class PrPlayerActivityGuard
    {
        public const int MAX_INACTIVITY_TICKS = 100;
        public const int MAX_ALL_TICKS = 1000;

        private class ActivityData
        {
            private int mLastHP;
            private int mLastGatheredCrystals;
            private int mLastCrystals;

            public bool OnTickFinished(PrPlayerFO player)
            {
                bool changed = false;
                if (player.healthPoints != mLastHP)
                {
                    changed = true;
                    mLastHP = player.healthPoints;
                }
                if (player.crystalsCount != mLastCrystals)
                {
                    changed = true;
                    mLastCrystals = player.crystalsCount;
                }
                if (player.gatheredCrystals != mLastGatheredCrystals)
                {
                    changed = true;
                    mLastGatheredCrystals = player.gatheredCrystals;
                }
                return changed;
            }
        }

        private int mInactivityCounter;
        private int mAllTicksCounter;
        private Dictionary<int, ActivityData> mActivityData;

        public int allTicksCounter
        {
            get { return mAllTicksCounter; }
        }

        public int inactivityCounter
        {
            get { return mInactivityCounter; }
        }

        public int OnTickFinished(IList<PrPlayerFO> players)
        {
            if (mActivityData == null)
            {
                mActivityData = new Dictionary<int, ActivityData>();
                foreach (PrPlayerFO player in players)
                {
                    mActivityData.Add(player.internalID, new ActivityData());
                }
            }


            int winPlayerID = -1;
            bool anyChanged = false;
            for (int i = 0; i < players.Count; ++i)
            {
                if (mActivityData[players[i].internalID].OnTickFinished(players[i]))
                {
                    anyChanged = true;
                }
            }
            ++mAllTicksCounter;
            if (anyChanged)
            {
                mInactivityCounter = 0;
            }
            else
            {
                ++mInactivityCounter;
            }
            if (mInactivityCounter > MAX_INACTIVITY_TICKS || mAllTicksCounter > MAX_ALL_TICKS)
            {
                if (players[0].gatheredCrystals != players[1].gatheredCrystals)
                {
                    winPlayerID = players[0].gatheredCrystals > players[1].gatheredCrystals ? players[0].internalID : players[1].internalID;
                }
                else if (players[0].crystalsCount != players[1].crystalsCount)
                {
                    winPlayerID = players[0].crystalsCount > players[1].crystalsCount ? players[0].internalID : players[1].internalID;
                }
                else if (players[0].healthPoints != players[1].healthPoints)
                {
                    winPlayerID = players[0].healthPoints > players[1].healthPoints ? players[0].internalID : players[1].internalID;
                }
                else
                {
                    winPlayerID = players[0].internalID > players[1].internalID ? players[0].internalID : players[1].internalID;
                }
            }
            return winPlayerID;
        }
    }

}
