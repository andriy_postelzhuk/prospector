﻿
namespace prospector.logic.actions
{

    public class PrTakeCrystalAction : PrBattleAction
    {

        public PrTakeCrystalAction(int playerID) :
            base(playerID)
        {
        }

        protected override void AppendCommandInfo(PrMessageBuilder mb)
        {
            mb.AppendLine("[TakeCrystalAction]");
        }

        protected override void DoAction(PrGameField gameField, PrGameEvents events)
        {
            gameField.DoATakingCrystalAction(playerID);
        }

        protected override bool IsCanDo(PrGameField gameField, PrMessageBuilder mb)
        {
            return gameField.CanDoATakingCrystalAction(playerID, mb);
        }
    }

}
