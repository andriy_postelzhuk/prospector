﻿using UnityEngine;
using UnityEngine.UI;

namespace prospector.view
{

    public class PrLogText : MonoBehaviour
    {

        public Text label;

        public void OnTickFinished(int playerID)
        {
            label.text = logic.PrGameLog.GetLogsForPlayer(playerID);
        }
    }

}
