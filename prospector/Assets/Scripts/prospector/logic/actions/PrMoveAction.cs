﻿using prospector.logic.fieldObjects.player;

namespace prospector.logic.actions
{

    public class PrMoveAction : PrBattleAction
    {

        private readonly EPrMoveDir mMoveDir;

        public PrMoveAction(int playerID, EPrMoveDir moveDir) :
            base(playerID)
        {
            mMoveDir = moveDir;
        }

        protected override void AppendCommandInfo(PrMessageBuilder mb)
        {
            mb.AppendLine("[MoveAction] " + mMoveDir);
        }

        protected override void DoAction(PrGameField gameField, PrGameEvents events)
        {
            gameField.DoMovePlayerAction(playerID, mMoveDir);
        }

        protected override bool IsCanDo(PrGameField gameField, PrMessageBuilder mb)
        {
            return gameField.CanDoAMoveAction(playerID, mMoveDir, mb);
        }
    }

}
