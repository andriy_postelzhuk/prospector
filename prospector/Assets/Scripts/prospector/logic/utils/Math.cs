﻿
namespace prospector.logic.utils
{

    public static class Math
    {
        public static int Abs(int value)
        {
            if (value < 0)
            {
                return -value;
            }
            return value;
        }
    }

}
