﻿using System.Collections.Generic;
using prospector.logic.data;
using TinyJSON;

namespace HTTPServerApp
{
    class ProspectorLogic
    {

        private const string KEY_ACTION = "action";
        private const string KEY_MOVE_DIR = "moveDir";
        private const string KEY_STAT = "stat";

        private enum EBattleActions
        {
            MOVE,
            ATTACK,
            TAKE_A_CRYSTAL
        };

        private enum EPrGameState
        {
            CHOOSING_STATS,
            BATTLE
        };

        public enum EPrStat
        {
            HEALTH,
            AGILITY,
            ATTACK
        };

        public enum EPrMoveDir
        {
            UP,
            DOWN,
            LEFT,
            RIGHT
        }

        private class RequestData
        {
            public int yourID;
            public EPrGameState gameState;
        }

        private class ImproveRequestData : RequestData
        {
            public Dictionary<int, PrPlayerStatsData> playerStats;
        }

        private class BattleRequestData : RequestData
        {
            public PrGameFieldData gameField;
        }

        private class BattleAction
        {
            public EBattleActions action;

            public BattleAction(EBattleActions ac)
            {
                action = ac;
            }
        }

        private class MoveAction : BattleAction
        {
            public MoveAction(EPrMoveDir moveDir) :
                base(EBattleActions.MOVE)
            {
                this.moveDir = moveDir;
            }

            public EPrMoveDir moveDir;
        }

        private class ImproveStatAction
        {
            public EPrStat stat;
        }


        public static string Run(string jsonStr)
        {
            Variant json = JSON.Load(jsonStr);
            EPrGameState gameState = json["gameState"].Make<EPrGameState>();

            if (gameState == EPrGameState.CHOOSING_STATS)
            {
                ImproveRequestData reqData = json.Make<ImproveRequestData>();
                int myID = reqData.yourID;
                EPrStat stat = EPrStat.AGILITY;
                PrPlayerStatsData statsData = reqData.playerStats[myID];
                if (statsData.agility > statsData.health)
                {
                    //stat = EPrStat.HEALTH;
                }
                ImproveStatAction statAction = new ImproveStatAction();
                statAction.stat = stat;
                return JSON.Dump(statAction, EncodeOptions.NoTypeHints);
            }
            else
            {
                BattleRequestData reqData = json.Make<BattleRequestData>();
                int myID = reqData.yourID;
                PrPlayerData player = null;
                foreach (PrPlayerData pd in reqData.gameField.players)
                {
                    if (pd.id == myID)
                    {
                        player = pd;
                        break;
                    }
                }

                sCurPlayerPos = player.position;

                List<BattleAction> actions = new List<BattleAction>();

                List<PrFieldObjectData> crystals = new List<PrFieldObjectData>(reqData.gameField.crystals);
                if (crystals.Count > 0)
                {
                    crystals.Sort(SortCrystals);
                    int crystalConter = 0;
                    while (actions.Count < player.actionsPoints)
                    {
                        prospector.logic.utils.SIntVector nearestCrystalPos = crystals[crystalConter].position;
                        int dist = nearestCrystalPos.ManhattanDistance(sCurPlayerPos);
                        if (dist == 0)
                        {
                            actions.Add(new BattleAction(EBattleActions.TAKE_A_CRYSTAL));
                            ++crystalConter;
                            if (crystalConter >= crystals.Count)
                            {
                                break;
                            }
                        }
                        else if (nearestCrystalPos.x > sCurPlayerPos.x)
                        {
                            actions.Add(new MoveAction(EPrMoveDir.RIGHT));
                            sCurPlayerPos.x++;
                        }
                        else if (nearestCrystalPos.x < sCurPlayerPos.x)
                        {
                            actions.Add(new MoveAction(EPrMoveDir.LEFT));
                            sCurPlayerPos.x--;
                        }
                        else if (nearestCrystalPos.y > sCurPlayerPos.y)
                        {
                            actions.Add(new MoveAction(EPrMoveDir.DOWN));
                            sCurPlayerPos.y++;
                        }
                        else
                        {
                            actions.Add(new MoveAction(EPrMoveDir.UP));
                            sCurPlayerPos.y--;
                        }
                    }

                }

                while (actions.Count < player.actionsPoints)
                {
                    actions.Add(new BattleAction(EBattleActions.ATTACK));
                }
                return JSON.Dump(actions, EncodeOptions.NoTypeHints);
            }
        }

        private static prospector.logic.utils.SIntVector sCurPlayerPos;

        private static int SortCrystals(PrFieldObjectData cr1, PrFieldObjectData cr2)
        {
            int d1 = cr1.position.ManhattanDistance(sCurPlayerPos);
            int d2 = cr2.position.ManhattanDistance(sCurPlayerPos);
            return d1.CompareTo(d2);
        }
    }
}
