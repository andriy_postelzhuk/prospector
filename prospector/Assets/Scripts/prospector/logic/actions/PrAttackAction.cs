﻿
using System;

namespace prospector.logic.actions
{

    public class PrAttackAction : PrBattleAction
    {

        public PrAttackAction(int playerID) :
            base(playerID)
        {
        }

        protected override void DoAction(PrGameField gameField, PrGameEvents events)
        {
            gameField.DoAnAttackAction(playerID, events);
        }

        protected override bool IsCanDo(PrGameField gameField, PrMessageBuilder mb)
        {
            return gameField.CanDoAnAttackAction(playerID, mb);
        }

        protected override void AppendCommandInfo(PrMessageBuilder mb)
        {
            mb.AppendLine("[AttackAction]");
        }
    }

}
