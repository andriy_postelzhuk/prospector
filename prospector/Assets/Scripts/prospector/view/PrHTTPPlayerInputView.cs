﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

using TinyJSON;

namespace prospector.view
{

    public class PrHTTPPlayerInputView : MonoBehaviour
    {

        private string KEY_SAVED_PLAYERS = "HTTPPlayerINputView.SavedPlayers";

        private struct SSavedPlayer
        {
            public string url;
            public string avatar;
            public string name;
        }

        public PrPlayerChooseScrollView allHttpPlayers;

        public GameObject allControlsContainer;
        public GameObject allInputContainer;
        public InputField urlInput;
        public InputField avatarInput;
        public InputField nameInput;

        public Button addButton;
        public Button removeButton;

        private Dictionary<string, SSavedPlayer> mAllPlayers;

        private PrPlayerList mPlayerList;

        private static void PhantomFunction()
        {
            JSON.SupportTypeForAOT<SSavedPlayer>();
        }

        public void Init(PrPlayerList playerList)
        {
            mPlayerList = playerList;
            try
            {
                string jsonStr = PlayerPrefs.GetString(KEY_SAVED_PLAYERS);
                if (!string.IsNullOrEmpty(jsonStr))
                {
                    Variant json = JSON.Load(jsonStr);
                    if (json != null)
                    {
                        mAllPlayers = json.Make<Dictionary<string, SSavedPlayer>>();
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
            
            if (mAllPlayers == null)
            {
                mAllPlayers = new Dictionary<string, SSavedPlayer>();
            }

            allHttpPlayers.AddPlayer(PrLocalization.BUTTON_LABEL_ADD_PLAYER);
            int viewSeed = 1;
            foreach (var pair in mAllPlayers)
            {
                allHttpPlayers.AddPlayer(pair.Key);
                AddHTTPPlayer(pair.Value, viewSeed);
                ++viewSeed;
            }
            UpdateInterfaceView();
            allHttpPlayers.NewPlayerChosenEvent += OnNewPlayerChosen;

            addButton.onClick.AddListener(AddPlayer);
            removeButton.onClick.AddListener(RemovePlayer);
        }

        private void RemovePlayer()
        {
            mPlayerList.RemoveHTTPPlayer(allHttpPlayers.chosenPlayerName);
            bool removed = mAllPlayers.Remove(allHttpPlayers.chosenPlayerName);
            logic.utils.Assert.IsTrue(removed);
            SaveDataToPrefs();
            allHttpPlayers.RemovePlayer(allHttpPlayers.chosenPlayerName);
        }

        private void AddPlayer()
        {
            if (IsInvalidInput())
            {
                return;
            }
            SSavedPlayer newPlayer = new SSavedPlayer();
            newPlayer.url = urlInput.text;
            newPlayer.avatar = avatarInput.text;
            newPlayer.name = nameInput.text;
            mAllPlayers.Add(newPlayer.name, newPlayer);

            SaveDataToPrefs();

            AddHTTPPlayer(newPlayer, mAllPlayers.Count);
            allHttpPlayers.AddPlayer(newPlayer.name);

            urlInput.text = "";
            nameInput.text = "";
            avatarInput.text = "";
        }

        private void SaveDataToPrefs()
        {
            try
            {
                PlayerPrefs.SetString(KEY_SAVED_PLAYERS, JSON.Dump(mAllPlayers, EncodeOptions.NoTypeHints));
                PlayerPrefs.Save();
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void AddHTTPPlayer(SSavedPlayer savePlayer, int viewSeed)
        {
            mPlayerList.AddHTTPPlayer(savePlayer.name, savePlayer.url, savePlayer.avatar, viewSeed);
        }

        private bool IsInvalidInput()
        {
            if (mAllPlayers.ContainsKey(nameInput.text))
            {
                nameInput.text = "";
                return true;
            }
            if (urlInput.text.Length < 4)
            {
                urlInput.text = "";
                return true;
            }
            return false;
        }

        private void OnNewPlayerChosen(PrPlayerChooseScrollView scrollView)
        {
            UpdateInterfaceView();
        }

        private void UpdateInterfaceView()
        {
            string selectedPlayer = allHttpPlayers.chosenPlayerName;
            if (string.IsNullOrEmpty(selectedPlayer))
            {
                allControlsContainer.SetActive(false);
                return;
            }
            allControlsContainer.SetActive(true);
            bool showAddBut = selectedPlayer == PrLocalization.BUTTON_LABEL_ADD_PLAYER;
            allInputContainer.SetActive(showAddBut);
            addButton.gameObject.SetActive(showAddBut);
            removeButton.gameObject.SetActive(!showAddBut);
        }
    }

}
