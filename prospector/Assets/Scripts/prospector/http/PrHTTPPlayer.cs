﻿using System.Collections.Generic;
using TinyJSON;

namespace prospector.http
{

    public class PrHTTPPlayer : logic.PrBasePlayer
    {

        private readonly PrHTTPRequest mRequest;
        private bool mIsBattleAction;
        private List<logic.actions.PrBattleAction> mSavedBattleActions;
        private string mAvatarURL;
        private int mViewSeed;

        private static void PhantomFunction()
        {
            JSON.SupportTypeForAOT<logic.data.PrPlayerStatsData>();
            JSON.SupportTypeForAOT<logic.data.PrFieldObjectData>();
            JSON.SupportTypeForAOT<logic.data.PrPlayerData>();
            JSON.SupportTypeForAOT<logic.data.PrGameFieldData>();
            JSON.SupportTypeForAOT<ImproveRequestData>();
            JSON.SupportTypeForAOT<BattleRequestData>();
        }

        public PrHTTPPlayer(int id, string name, string serverURL, string avatarURL, int viewSeed) :
            base(id, name)
        {
            mAvatarURL = avatarURL;
            mViewSeed = viewSeed;
            mRequest = new PrHTTPRequest(serverURL);
            mRequest.OnRequestComplete += OnRequestComplete;
            mRequest.OnRequestError += OnRequestError;
        }

        public override logic.IPrPlayer Clone(int id)
        {
            return new PrHTTPPlayer(id, pureName, mRequest.url, mAvatarURL, mViewSeed);
        }

        public override string avatarURL
        {
            get { return mAvatarURL; }
        }

        public override int viewSeed
        {
            get { return mViewSeed; }
        }

        protected override void GenerateBattleAction()
        {
            if (mSavedBattleActions == null)
            {
                mSavedBattleActions = new List<logic.actions.PrBattleAction>();
            }
            if (mSavedBattleActions.Count > 0)
            {
                OnBattleRequestFinished();
                return;
            }

            mIsBattleAction = true;
            while (mSavedBattleActions.Count < player.actionPoints)
            {
                mSavedBattleActions.Add(null);
            }

            BattleRequestData reqData = new BattleRequestData();
            reqData.gameField = mGameField.GenerateData();
            SendRequest(reqData);
        }

        protected override void GenerateImproveAction()
        {
            mIsBattleAction = false;
            ImproveRequestData reqData = new ImproveRequestData();
            reqData.playerStats = new Dictionary<int, logic.data.PrPlayerStatsData>();
            reqData.playerStats[id] = player.stats.GenerateData();
            reqData.playerStats[enemy.internalID] = enemy.stats.GenerateData();
            SendRequest(reqData);
        }

        private void SendRequest(RequestData reqData)
        {
            reqData.yourID = id;
            reqData.gameState = mIsBattleAction ? logic.EPrGameState.BATTLE : logic.EPrGameState.CHOOSING_STATS;
            mRequest.SendRequest(JSON.Dump(reqData, EncodeOptions.NoTypeHints));
        }

        private void OnRequestComplete(string text, float reqTime)
        {
            logic.PrGameLog.AddLogForPlayer(id, string.Format("{0} / {1}\nRequest complete:\n{2}", reqTime, mRequest.allRequestTime, text));
            IList<string> parseErrors = null;
            if (mIsBattleAction)
            {
                parseErrors = PrCommandsParser.ParseBattleCommands(id, mSavedBattleActions, text);
                OnBattleRequestFinished();
            }
            else
            {
                logic.actions.PrImproveStatAction improveAction;
                parseErrors = PrCommandsParser.ParseImproveCommand(id, out improveAction, text);
                SetReadyImproveAction(improveAction);
            }
            foreach (string message in parseErrors)
            {
                logic.PrGameLog.AddLogForPlayer(id, "Parse error: " + message);
            }
        }

        private void OnRequestError(string text, float reqTime)
        {
            logic.PrGameLog.AddLogForPlayer(id, string.Format("{0} / {1}\nRequest errorHTTP error:\n{2}", reqTime, mRequest.allRequestTime, text));
            if (mIsBattleAction)
            {
                OnBattleRequestFinished();
            }
            else
            {
                SetReadyImproveAction(null);
            }
        }

        private void OnBattleRequestFinished()
        {
            logic.utils.Assert.IsTrue(mSavedBattleActions.Count == player.actionPoints);
            logic.actions.PrBattleAction batAction = mSavedBattleActions[0];
            mSavedBattleActions.RemoveAt(0);
            SetReadyBattleAction(batAction);
        }

        private class RequestData
        {
            public int yourID;
            public logic.EPrGameState gameState;
        }

        private class ImproveRequestData : RequestData
        {
            public Dictionary<int, logic.data.PrPlayerStatsData> playerStats;
        }

        private class BattleRequestData : RequestData
        {
            public logic.data.PrGameFieldData gameField;
        }
    }
}
