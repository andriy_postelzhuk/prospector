﻿
namespace prospector.logic.actions
{

    public abstract class PrBattleAction : PrBaseAction
    {

        public PrBattleAction(int playerID) :
            base(playerID)
        {
        }

        protected override void DoFallbackAction(PrGameField gameField, PrGameEvents events)
        {
            utils.Assert.IsTrue(false);
        }

        protected override bool IsCanDoFallback(PrGameField gameField, PrMessageBuilder mb)
        {
            return false;
        }
    }

}
