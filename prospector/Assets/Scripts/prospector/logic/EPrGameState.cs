﻿
namespace prospector.logic
{

    public enum EPrGameState
    {
        Undefined,
        CHOOSING_STATS,
        BATTLE,
        FINISHED
    }

}
