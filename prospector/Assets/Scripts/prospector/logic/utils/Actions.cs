﻿
namespace prospector.logic.utils
{

    public static class Action
    {
        public static void Call(this System.Action action)
        {
            if (action != null)
            {
                action();
            }
        }

        public static void Call<T>(this System.Action<T> action, T p1)
        {
            if (action != null)
            {
                action(p1);
            }
        }

        public static void Call<T, U>(this System.Action<T, U> action, T p1, U p2)
        {
            if (action != null)
            {
                action(p1, p2);
            }
        }
    }

}
