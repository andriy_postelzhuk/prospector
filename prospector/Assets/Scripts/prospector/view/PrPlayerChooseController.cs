﻿using UnityEngine;
using UnityEngine.UI;

using prospector.logic;

namespace prospector.view
{

    public class PrPlayerChooseController : MonoBehaviour
    {
        public PrPlayerChooseScrollView[] chooseScrollViews;
        public Button startGameButton;
        public PrHTTPPlayerInputView httpPlayers;

        private string[] mChosenPlayers;
        private PrPlayerList mPlayerList;

        void Start()
        {
            startGameButton.GetComponentInChildren<Text>().text = PrLocalization.BUTTON_LABEL_START_GAME;
            startGameButton.onClick.AddListener(OnClickStartButton);

            mPlayerList = new PrPlayerList();
            //TODO: implement add HTTP player interface
            mPlayerList.NewPlayerEvent += OnNewPlayerAdded;
            mPlayerList.RemovePlayerEvent += OnPlayerRemoved;
            mChosenPlayers = new string[chooseScrollViews.Length];
            UpdateStartGameButton();
            foreach (var scrollView in chooseScrollViews)
            {
                foreach (string n in mPlayerList.allPlayerNames)
                {
                    scrollView.AddPlayer(n);
                }
                scrollView.NewPlayerChosenEvent += OnNewPlayerChosen;
            }
            httpPlayers.Init(mPlayerList);
        }

        private void OnNewPlayerChosen(PrPlayerChooseScrollView scrollView)
        {
            for (int i = 0; i < chooseScrollViews.Length; ++i)
            {
                mChosenPlayers[i] = chooseScrollViews[i].chosenPlayerName;
            }
            UpdateStartGameButton();
        }

        private void OnNewPlayerAdded(string name)
        {
            foreach (var scrollView in chooseScrollViews)
            {
                scrollView.AddPlayer(name);
            }
        }

        private void OnPlayerRemoved(string name)
        {
            foreach (var scrollView in chooseScrollViews)
            {
                scrollView.RemovePlayer(name);
            }
        }

        private void UpdateStartGameButton()
        {
            bool startButtonEnabled = true;
            foreach (var name in mChosenPlayers)
            {
                if (string.IsNullOrEmpty(name))
                {
                    startButtonEnabled = false;
                    break;
                }
            }
            startGameButton.gameObject.SetActive(startButtonEnabled);
        }

        private void OnClickStartButton()
        {
            IPrPlayer[] players = new IPrPlayer[mChosenPlayers.Length];
            for (int i = 0; i < mChosenPlayers.Length; ++i)
            {
                players[i] = mPlayerList.CreatePlayer(i + 1, mChosenPlayers[i]);
            }
            PrGameFlow.StartGame(players);
        }
    }

}
