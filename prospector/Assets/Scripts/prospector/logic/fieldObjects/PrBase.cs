﻿namespace prospector.logic.fieldObjects
{

    public class PrBase : PrFieldObject
    {

        public PrBase(int playerOwnerID, utils.SIntVector position) :
            base(playerOwnerID, position)
        {
        }

        public override EPrFOType typeID
        {
            get { return EPrFOType.BASE; }
        }

    }

}
