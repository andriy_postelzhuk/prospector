﻿using UnityEngine;
using UnityEngine.UI;

namespace prospector.view
{

    public class PrPlayerEventElement : MonoBehaviour
    {
        private const int SHOW_TICK_COUNTER = 4;

        public Text label;

        private int mTickCounter;

        public void Init(int eventValue, Color eventColor)
        {
            label.text = eventValue > 0 ? "+" + eventValue : eventValue.ToString();
            label.color = eventColor;
        }

        public bool OnTickFinished()
        {
            Vector2 pos = label.rectTransform.anchoredPosition;
            pos.y += label.rectTransform.sizeDelta.y;
            label.rectTransform.anchoredPosition = pos;
            ++mTickCounter;
            return SHOW_TICK_COUNTER <= mTickCounter;
        }
    }

}
