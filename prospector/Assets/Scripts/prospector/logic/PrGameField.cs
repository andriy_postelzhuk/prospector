﻿using System.Collections.Generic;
using prospector.logic.fieldObjects;
using prospector.logic.fieldObjects.player;

namespace prospector.logic
{

    public class PrGameField
    {

        private int mWidth;
        private int mHeight;
        private int mCrystallsCount;
        private PrCell[,] mField;
        private PrPlayerFO[] mPlayers;
        private PrBase[] mBases;
        private List<PrCrystal> mCrystals;

        public PrGameField(PrStartGameParams startParams)
        {
            mWidth = startParams.gameFieldSize.x;
            mHeight = startParams.gameFieldSize.y;

            BaseInit();

            //brut hard code starts here
            utils.Assert.IsTrue(startParams.playersNumber == 2); //brut hard code
            utils.SIntVector[] playersStartPositions = new utils.SIntVector[startParams.playersNumber];
            playersStartPositions[0] = new utils.SIntVector(0, 0);
            playersStartPositions[1] = new utils.SIntVector(mWidth - 1, mHeight - 1);
            //brut hard code finishes here

            mPlayers = new PrPlayerFO[startParams.playersNumber];
            mBases = new PrBase[startParams.playersNumber];
            for (int i = 0; i < startParams.playersNumber; ++i)
            {
                PrPlayerFO playerFO = new PrPlayerFO(startParams.playerInternalIDs[i], playersStartPositions[i], startParams.playerImproveStatPoints);
                AddPlayer(playerFO, i);

                PrBase baseFO = new PrBase(playerFO.internalID, playersStartPositions[i]);
                AddBase(baseFO, i);
            }

            if (!startParams.isDebug)
            {
                utils.Assert.IsTrue((startParams.crystalsPerPlayerNumber * startParams.playersNumber) < (mWidth * mHeight / 4));
                utils.Assert.IsTrue((mWidth % 2 == 1) && (mHeight % 2 == 1));
            }
            int halfHeight = mHeight / 2;
            mCrystals = new List<PrCrystal>();
            //Add odd crystall to the center
            AddCrystal(startParams.crystalsPerPlayerNumber * 2, mWidth / 2, halfHeight);

            //TODO: later implement random generation with server
            System.Random r = new System.Random(startParams.randomSeed);
            for (int i = 0; i < startParams.crystalsPerPlayerNumber; ++i)
            {
                for (;;)
                {
                    int x = r.Next(mWidth);
                    int y = r.Next(halfHeight);
                    if (!mField[x, y].hasAnyObjects)
                    {
                        AddCrystal(i * 2, x, y);
                        AddCrystal(i * 2 + 1, mWidth - x - 1, mHeight - y - 1);
                        break;
                    }
                }
            }
        }

        public PrGameField(data.PrGameFieldData gfData)
        {
            mWidth = gfData.width;
            mHeight = gfData.height;
            BaseInit();

            mPlayers = new PrPlayerFO[gfData.players.Length];
            for (int i = 0; i < mPlayers.Length; ++i)
            {
                AddPlayer(new PrPlayerFO(gfData.players[i]), i);
            }

            mBases = new PrBase[gfData.bases.Length];
            for (int i = 0; i < mBases.Length; ++i)
            {
                AddBase(new PrBase(gfData.bases[i].id, gfData.bases[i].position), i);
            }

            mCrystals = new List<PrCrystal>();
            for (int i = 0; i < gfData.crystals.Length; ++i)
            {
                utils.SIntVector pos = gfData.crystals[i].position;
                AddCrystal(gfData.crystals[i].id, pos.x, pos.y);
            }
        }

        private void BaseInit()
        {
            mField = new PrCell[mWidth, mHeight];
            for (int i = 0; i < mWidth; ++i)
            {
                for (int j = 0; j < mHeight; ++j)
                {
                    mField[i, j] = new PrCell();
                }
            }
        }

        public data.PrGameFieldData GenerateData()
        {
            data.PrGameFieldData gfData = new data.PrGameFieldData();
            gfData.width = mWidth;
            gfData.height = mHeight;

            gfData.crystals = new prospector.logic.data.PrFieldObjectData[mCrystals.Count];
            for (int i = 0; i < mCrystals.Count; ++i)
            {
                gfData.crystals[i] = mCrystals[i].GenerateFOData();
            }

            gfData.players = new data.PrPlayerData[mPlayers.Length];
            for (int i = 0; i < mPlayers.Length; ++i)
            {
                gfData.players[i] = mPlayers[i].GenerateData();
            }

            gfData.bases = new data.PrFieldObjectData[mBases.Length];
            for (int i = 0; i < mBases.Length; ++i)
            {
                gfData.bases[i] = mBases[i].GenerateFOData();
            }

            return gfData;
        }

        public int halfCrystalsCount
        {
            get
            {
                int allCrystalsCount = mCrystals.Count;
                foreach (var player in mPlayers)
                {
                    allCrystalsCount += player.crystalsCount;
                    allCrystalsCount += player.gatheredCrystals;
                }
                return allCrystalsCount / 2;
            }
        }

        public PrCell[,] cells
        {
            get { return mField; }
        }

        public IList<PrPlayerFO> players
        {
            get { return mPlayers; }
        }

        public PrPlayerFO GetPlayerByID(int id)
        {
            foreach (PrPlayerFO player in mPlayers)
            {
                if (player.internalID == id)
                {
                    return player;
                }
            }
            return null;
        }

        public bool IsAllPlayersStatsChosen()
        {
            foreach (PrPlayerFO player in mPlayers)
            {
                if (player.stats.CanImproveAnySkill())
                {
                    return false;
                }
            }
            return true;
        }

        public void StartBattle()
        {
            foreach (PrPlayerFO player in mPlayers)
            {
                player.OnStartBattle();
            }
        }

        public void AllPlayersFinishTurn(PrGameEvents events)
        {
            foreach (PrBase baseFO in mBases)
            {
                foreach (PrPlayerFO player in mPlayers)
                {
                    if (player.isDead || player.position != baseFO.position)
                    {
                        continue;
                    }
                    int hpDelta = 0;
                    if (player.internalID == baseFO.internalID)
                    {
                        hpDelta = player.ChangeHealthPoints(PrAllConsts.HEAL_STRENGTH_OF_BASE);
                    }
                    else
                    {
                        hpDelta = player.ChangeHealthPoints(-PrAllConsts.ATTACK_STRENGTH_OF_BASE);
                    }
                    events.AddChangeHPEvent(player.internalID, hpDelta);
                }
            }
        }

        public bool CheckAllObjectsInCells()
        {
            for (int x = 0; x < mWidth; ++x)
            {
                for (int y = 0; y < mHeight; ++y)
                {
                    PrCell cell = mField[x, y];
                    utils.SIntVector pos = new utils.SIntVector(x, y);
                    foreach (var pair in cell.allObjects)
                    {
                        if (pair.Value.position != pos)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public bool CanDoAMoveAction(int playerID, EPrMoveDir moveDir, PrMessageBuilder mb)
        {
            if (mb == null)
            {
                mb = PrMessageBuilder.nullBuilder;
            }
            if (moveDir == EPrMoveDir.Undefined)
            {
                mb.AppendLine("Undefined move direction");
                return false;
            }
            PrPlayerFO player = GetPlayerByID(playerID);
            if (!player.canDoAnAction)
            {
                mb.AppendLine("Player can't do any action");
                return false;
            }
            utils.SIntVector moveVec = PrMoveDirUtils.MoveVector(moveDir);
            utils.SIntVector curPos = player.position + moveVec;
            if (curPos.x < 0 || curPos.y < 0 ||curPos.x >= mWidth || curPos.y >= mHeight)
            {
                mb.AppendLine("Target cell out of field");
                return false;
            }
            PrCell cell = mField[curPos.x, curPos.y];
            if (cell.HasObjectOfType(EPrFOType.PLAYER))
            {
                mb.AppendLine("Target cell has another player");
                return false; //another player
            }
            return true;
        }

        public void DoMovePlayerAction(int playerID, EPrMoveDir moveDir)
        {
            utils.Assert.IsTrue(CanDoAMoveAction(playerID, moveDir, null));
            PrPlayerFO player = GetPlayerByID(playerID);

            PrCell curCell = mField[player.position.x, player.position.y];
            curCell.RemoveFO(player);

            player.MoveAction(moveDir);
            PrCell newCell = mField[player.position.x, player.position.y];
            newCell.AddFO(player);

            PrBase baseFO = GetPlayerBase(playerID);
            if (baseFO.position == player.position)
            {
                player.PutCrystalls();
            }
        }

        public bool CanDoAnAttackAction(int playerID, PrMessageBuilder mb)
        {
            if (mb == null)
            {
                mb = PrMessageBuilder.nullBuilder;
            }
            PrPlayerFO player = GetPlayerByID(playerID);
            if (!player.canDoAnAction)
            {
                mb.AppendLine("Player can't do any action");
                return false;
            }
            PrPlayerFO enemyPlayer = GetEnemyPlayerFor(playerID);
            if (enemyPlayer.isDead)
            {
                mb.AppendLine("Enemy is dead");
                return false;
            }
            if (enemyPlayer.position.ManhattanDistance(player.position) > PrAllConsts.ATTACK_RANGE)
            {
                mb.AppendLine("Enemy is too far away");
                return false;
            }
            return true;
        }

        public void DoAnAttackAction(int playerID, PrGameEvents events)
        {
            utils.Assert.IsTrue(CanDoAnAttackAction(playerID, null));
            PrPlayerFO player = GetPlayerByID(playerID);
            PrPlayerFO enemyPlayer = GetEnemyPlayerFor(playerID);
            player.AttackAction();
            events.AddAttackEvent(playerID, player.stats.attack);
            int damage = enemyPlayer.ChangeHealthPoints(-player.stats.attack);
            events.AddChangeHPEvent(enemyPlayer.internalID, damage);
        }

        public bool CanDoATakingCrystalAction(int playerID, PrMessageBuilder mb)
        {
            if (mb == null)
            {
                mb = PrMessageBuilder.nullBuilder;
            }
            PrPlayerFO player = GetPlayerByID(playerID);
            if (!player.canDoAnAction)
            {
                mb.AppendLine("Player can't do any action");
                return false;
            }
            PrCell cell = mField[player.position.x, player.position.y];
            if (!cell.HasObjectOfType(EPrFOType.CRYSTAL))
            {
                mb.AppendLine("Current cell doesn't countain a crystal");
                return false;
            }
            return true;
        }

        public void DoATakingCrystalAction(int playerID)
        {
            utils.Assert.IsTrue(CanDoATakingCrystalAction(playerID, null));
            PrPlayerFO player = GetPlayerByID(playerID);
            PrCell cell = mField[player.position.x, player.position.y];
            PrCrystal crystal = cell.GetObjectOfType(EPrFOType.CRYSTAL) as PrCrystal;
            cell.RemoveFO(crystal);
            bool removed = mCrystals.Remove(crystal);
            utils.Assert.IsTrue(removed);
            player.TakeCrystalAction();
        }

        public IList<PrCrystal> allCrystals
        {
            get { return mCrystals; }
        }

        public PrPlayerFO GetEnemyPlayerFor(int playerID)
        {
            foreach (PrPlayerFO player in mPlayers)
            {
                if (player.internalID != playerID)
                {
                    return player;
                }
            }
            utils.Assert.IsTrue(false);
            return null;
        }

        public PrBase GetPlayerBase(int playerID)
        {
            foreach (PrBase baseFO in mBases)
            {
                if (baseFO.internalID == playerID)
                {
                    return baseFO;
                }
            }
            utils.Assert.IsTrue(false);
            return null;
        }

        public PrBase GetEnemyPlayerBase(int playerID)
        {
            foreach (PrBase baseFO in mBases)
            {
                if (baseFO.internalID != playerID)
                {
                    return baseFO;
                }
            }
            utils.Assert.IsTrue(false);
            return null;
        }

        private void AddPlayer(PrPlayerFO player, int index)
        {
            mPlayers[index] = player;
            player.OnPlayerDied += OnPlayerDied;
            AddObject(player);
        }

        private void AddCrystal(int id, int x, int y)
        {
            PrCrystal crystal = new PrCrystal(id, new utils.SIntVector(x, y));
            mCrystals.Add(crystal);
            AddObject(crystal);
        }

        private void AddBase(PrBase baseFO, int index)
        {
            mBases[index] = baseFO;
            AddObject(baseFO);
        }

        private void AddObject(PrFieldObject fo)
        {
            mField[fo.position.x, fo.position.y].AddFO(fo);
        }

        private void OnPlayerDied(PrPlayerFO player)
        {
            mField[player.position.x, player.position.y].RemoveFO(player);
        }
    }

}
