﻿using UnityEngine;
using prospector.logic.utils;

namespace prospector.http
{

    public class PrHTTPRequest
    {
        private const float PING_TIME = 0.2f;
        private const float MAX_SECONDS_PER_ONE_REQUEST = 10.0f;
        private const float MAX_SECONDS_FOR_ALL_REQUESTS = 70.0f;

        private readonly string mURL;
        private WWW mWWW;
        private float mStartRequestTime;
        private float mAllRequestTime;

        public PrHTTPRequest(string url)
        {
            mURL = url;
        }

        public event System.Action<string, float> OnRequestComplete;
        public event System.Action<string, float> OnRequestError;

        public string url
        {
            get { return mURL; }
        }

        public float allRequestTime
        {
            get { return mAllRequestTime; }
        }

        public void SendRequest(string data)
        {
            if (mAllRequestTime > MAX_SECONDS_FOR_ALL_REQUESTS)
            {
                OnRequestError.Call("all requests timeout", 0.0f);
                return;
            }
            Assert.IsTrue(mWWW == null);
            mWWW = new WWW(mURL, System.Text.Encoding.UTF8.GetBytes(data));
            PrUpdateDispatcher.instance.OnUpdate += OnUpdate;
            mStartRequestTime = Time.realtimeSinceStartup;
        }

        private void OnUpdate()
        {
            float requestTime = Time.realtimeSinceStartup - mStartRequestTime;
            if (mWWW.isDone)
            {
                requestTime -= PING_TIME;
                if (requestTime > 0.0f)
                {
                    mAllRequestTime += requestTime;
                }
                else
                {
                    requestTime = 0.0f;
                }
                if (string.IsNullOrEmpty(mWWW.error))
                {
                    OnRequestComplete.Call(mWWW.text, requestTime);
                }
                else
                {
                    OnRequestError.Call(mWWW.error, requestTime);
                }
                ClearRequest();
            }
            else if (requestTime > MAX_SECONDS_PER_ONE_REQUEST)
            {
                ClearRequest();
                mAllRequestTime += MAX_SECONDS_PER_ONE_REQUEST;
                OnRequestError.Call("one request timout", MAX_SECONDS_PER_ONE_REQUEST);
            }
        }

        private void ClearRequest()
        {
            mWWW.Dispose();
            mWWW = null;
            PrUpdateDispatcher.instance.OnUpdate -= OnUpdate;
        }
    }
}
