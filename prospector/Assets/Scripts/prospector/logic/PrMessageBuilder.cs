﻿using System.Text;

namespace prospector.logic
{

    public class PrMessageBuilder
    {
        public static PrMessageBuilder nullBuilder
        {
            get
            {
                if (sNullBuilder == null)
                {
                    sNullBuilder = new PrMessageBuilder(false);
                }
                return sNullBuilder;
            }
        }

        private static PrMessageBuilder sNullBuilder;

        private StringBuilder mSB;

        public PrMessageBuilder(bool logEnabled)
        {
            if (logEnabled)
            {
                mSB = new StringBuilder();
            }
        }

        public void AppendLine(string line)
        {
            if (mSB != null)
            {
                mSB.AppendLine(line);
            }
        }

        public string GetAllMessage()
        {
            if (mSB == null)
            {
                return null;
            }
            return mSB.ToString();
        }
    }

}
