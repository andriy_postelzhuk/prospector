﻿
namespace prospector.logic.utils
{

public static class Log
{
    public static void i(string message)
    {
#if UNITY_5_0
            //TODO: temporary
            //implement like timer
            UnityEngine.Debug.Log(message);
#else
            throw new System.NotImplementedException();
#endif
        }
}

}