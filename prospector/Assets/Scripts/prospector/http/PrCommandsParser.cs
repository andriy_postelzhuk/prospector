﻿using System.Collections.Generic;
using prospector.logic.actions;
using prospector.logic.fieldObjects.player;
using TinyJSON;

namespace prospector.http
{

    public static class PrCommandsParser
    {
        private const string KEY_ACTION = "action";
        private const string KEY_MOVE_DIR = "moveDir";
        private const string KEY_STAT = "stat";

        private enum EBattleActions
        {
            MOVE,
            ATTACK,
            TAKE_A_CRYSTAL
        };

        private static List<string> sParseMessages;

        public static IList<string> ParseBattleCommands(int playerID, List<PrBattleAction> actionsArr, string json)
        {
            try
            {
                Variant jsonResponse = GetJSON(json);
                if (jsonResponse == null)
                {
                    return sParseMessages;
                }
                ProxyArray jsonArr = jsonResponse as ProxyArray;
                if (jsonArr == null)
                {
                    sParseMessages.Add("Is not json array: " + json);
                    return sParseMessages;
                }

                if (jsonArr.Count != actionsArr.Count)
                {
                    sParseMessages.Add(string.Format("Response array unexpected length: {0} / {1}", jsonArr.Count, actionsArr.Count));
                }

                for (int i = 0; i < actionsArr.Count && i < jsonArr.Count; ++i)
                {
                    try
                    {
                        PrBattleAction battleAction = null;
                        Variant commandJson = jsonArr[i];
                        EBattleActions action = commandJson[KEY_ACTION].Make<EBattleActions>();
                        switch (action)
                        {
                            case EBattleActions.MOVE:
                                EPrMoveDir moveDir = commandJson[KEY_MOVE_DIR].Make<EPrMoveDir>();
                                battleAction = new PrMoveAction(playerID, moveDir);
                                break;
                            case EBattleActions.ATTACK:
                                battleAction = new PrAttackAction(playerID);
                                break;
                            case EBattleActions.TAKE_A_CRYSTAL:
                                battleAction = new PrTakeCrystalAction(playerID);
                                break;
                        }
                        actionsArr[i] = battleAction;
                    }
                    catch (System.Exception e)
                    {
                        sParseMessages.Add(e.Message);
                    }
                }

            }
            catch (System.Exception e)
            {
                sParseMessages.Add(e.Message);
            }

            return sParseMessages;
        }

        public static IList<string> ParseImproveCommand(int playerID, out PrImproveStatAction improveAction, string json)
        {
            improveAction = null;
            try
            {
                Variant jsonResponse = GetJSON(json);
                if (jsonResponse == null)
                {
                    return sParseMessages;
                }
                ProxyObject jsonObj = jsonResponse as ProxyObject;
                if (jsonObj == null)
                {
                    sParseMessages.Add("Is not json object: " + json);
                    return sParseMessages;
                }

                EPrStat stat = jsonObj[KEY_STAT].Make<EPrStat>();
                improveAction = new PrImproveStatAction(playerID, stat);
            }
            catch (System.Exception e)
            {
                sParseMessages.Add(e.Message);
            }

            return sParseMessages;
        }

        private static Variant GetJSON(string json)
        {
            if (sParseMessages == null)
            {
                sParseMessages = new List<string>();
            }
            else
            {
                sParseMessages.Clear();
            }
            Variant jsonResponse = JSON.Load(json);
            if (jsonResponse == null)
            {
                sParseMessages.Add("Not valid json: " + json);
            }
            return jsonResponse;
        }
    }

}
