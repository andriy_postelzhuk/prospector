﻿using prospector.logic.actions;

namespace prospector.logic.bots
{

    public abstract class PrBaseBot : PrBasePlayer
    {

        public PrBaseBot(int id, string name) :
            base(id, name)
        {
        }

        protected override void GenerateBattleAction()
        {
            SetReadyBattleAction(null);
        }

        protected override void GenerateImproveAction()
        {
            SetReadyImproveAction(new PrBaseImproveStatAction(id));
        }
        
    }

}
