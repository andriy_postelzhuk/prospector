﻿using System.Text;
using prospector.logic.utils;

namespace prospector.logic.test
{

public static class TestIntVector
{
    public static void Run(StringBuilder sb)
    {
            sb.AppendLine("===== TestIntVector Run =====");
            SIntVector v1 = new SIntVector(2, 2);
            SIntVector v2 = new SIntVector(2, 2);
            SIntVector v3 = new SIntVector(4, 5);
            Assert.IsTrue(v1 == v2);
            Assert.IsTrue(v2 != v3);
            Assert.IsTrue(v2 == v1);
            Assert.IsTrue(v3 != v1);
            v1 -= v2;
            Assert.IsTrue(v1 == new SIntVector(0, 0));
            Assert.IsTrue(v2 == new SIntVector(2, 2));
            Assert.IsTrue(v2 * 2 * 3 == v2 * 6);
            v1 = 2 * v2 + new SIntVector(0, 1);
            Assert.IsTrue(v1 == v3);
            Assert.IsTrue(v3.ManhattanDistance() == 9);
            v3 = -v3;
            Assert.IsTrue(v3.ManhattanDistance() == 9);
            Assert.IsTrue(v3.ManhattanDistance(v1) == 18);
            v3 += v1;
            Assert.IsTrue(v3 == v3 * 100);
            Assert.IsTrue(v1 * v2 == new SIntVector(8, 10));
            Assert.IsTrue(v1.ManhattanDistance(v2) == 5);
            sb.AppendLine("===== TestIntVector success =====");
        }
}

}
