﻿
namespace prospector.logic.utils
{

    public struct SIntVector
    {
        public int x;
        public int y;

        public SIntVector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int ManhattanDistance()
        {
            int d = x > 0 ? x : -x;
            d += (y > 0 ? y : -y);
            return d;
        }

        public int ManhattanDistance(SIntVector other)
        {
            return (this - other).ManhattanDistance();
        }

        public override string ToString()
        {
            return string.Format("[{0}:{1}]", x, y);
        }

        public override bool Equals(object obj)
        {
            SIntVector other = (SIntVector)obj;
            return this == other;
        }

        public override int GetHashCode()
        {
            return (x * 65536) ^ y;
        }

        public static SIntVector operator -(SIntVector v)
        {
            return new SIntVector(-v.x, -v.y);
        }

        public static SIntVector operator +(SIntVector v1, SIntVector v2)
        {
            return new SIntVector(v1.x + v2.x, v1.y + v2.y);
        }

        public static SIntVector operator -(SIntVector v1, SIntVector v2)
        {
            return new SIntVector(v1.x - v2.x, v1.y - v2.y);
        }

        public static SIntVector operator *(SIntVector v1, SIntVector v2)
        {
            return new SIntVector(v1.x * v2.x, v1.y * v2.y);
        }

        public static SIntVector operator *(SIntVector v1, int scalar)
        {
            return new SIntVector(v1.x * scalar, v1.y * scalar);
        }

        public static SIntVector operator *(int scalar, SIntVector v1)
        {
            return new SIntVector(v1.x * scalar, v1.y * scalar);
        }

        public static bool operator ==(SIntVector v1, SIntVector v2)
        {
            return v1.x == v2.x && v1.y == v2.y;
        }

        public static bool operator !=(SIntVector v1, SIntVector v2)
        {
            return !(v1 == v2);
        }
    }

}
