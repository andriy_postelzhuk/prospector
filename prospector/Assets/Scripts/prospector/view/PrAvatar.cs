﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace prospector.view
{

    public class PrAvatar : MonoBehaviour
    {
        //TODO: delete this url
        private const string DEF_URL = "http://www.readingfc.co.uk/images/common/bg_player_profile_default_big.png";

        public RawImage avatar;

        public void Load(string url)
        {
            StopAllCoroutines();
            if (string.IsNullOrEmpty(url))
            {
                url = DEF_URL;
            }
            StartCoroutine(LoadImage(url));
        }

        private IEnumerator LoadImage(string url)
        {
            WWW www = new WWW(url);
            yield return www;
            TrySetTextureFromWWW(www);
            www.Dispose();
            if (avatar.texture == null)
            {
                www = new WWW(DEF_URL);
                yield return www;
                TrySetTextureFromWWW(www);
                www.Dispose();
            }
        }

        private void TrySetTextureFromWWW(WWW www)
        {
            if (string.IsNullOrEmpty(www.error))
            {
                avatar.texture = www.textureNonReadable;
            }
        }
    }

}
