﻿using System;
using UnityEngine;

namespace prospector.http
{

    public class PrUpdateDispatcher : MonoBehaviour
    {
        private static PrUpdateDispatcher sInstance;

        public static PrUpdateDispatcher instance
        {
            get
            {
                if (sInstance == null)
                {
                    GameObject go = new GameObject();
                    sInstance = go.AddComponent<PrUpdateDispatcher>();
                }
                return sInstance;
            }
        }

        public event Action OnUpdate;

        // Update is called once per frame
        void Update()
        {
            if (OnUpdate != null)
            {
                OnUpdate();
            }
        }
    }


}
