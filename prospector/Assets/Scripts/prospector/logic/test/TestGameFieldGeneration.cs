﻿using System.Text;
using System.Collections.Generic;
using prospector.logic.fieldObjects;

namespace prospector.logic.test
{

    public static class TestGameFieldGeneration
    {
        public static PrStartGameParams CreateSimpleStartParams(int width = 7, int height = 5, int crystalsNumber = 3, int seed = -1)
        {
            PrStartGameParams startParams = PrStartGameParams.CreateParams(width, height, crystalsNumber, seed);
            startParams.playerInternalIDs = new int[] { 1, 2 };
            startParams.isDebug = true;
            return startParams;
        }

        public static int PrintField(PrCell[,] field, StringBuilder sb)
        {
            int allCrystallsCount = 0;
            sb.AppendLine();
            for (int j = 0; j < field.GetLength(1); ++j)
            {
                for (int i = 0; i < field.GetLength(0); ++i)
                {
                    IDictionary<EPrFOType, PrFieldObject> cellObjects = field[i, j].allObjects;
                    sb.Append("|_");
                    utils.Assert.IsTrue(cellObjects.Count <= 2);
                    foreach (var pair in cellObjects)
                    {
                        char c = '\0';
                        switch (pair.Key)
                        {
                            case EPrFOType.BASE:
                                c = 'b';
                                break;
                            case EPrFOType.CRYSTAL:
                                c = 'c';
                                ++allCrystallsCount;
                                break;
                            case EPrFOType.PLAYER:
                                c = 'p';
                                break;
                        }
                        utils.Assert.IsTrue(c != '\0');
                        sb.Append(c);
                    }
                    for (int k = cellObjects.Count; k < 3; ++k)
                    {
                        sb.Append('_');
                    }
                }
                sb.AppendLine();
            }
            return allCrystallsCount;
        }

        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestGameFieldGeneration Run =====");

            PrStartGameParams startParams = CreateSimpleStartParams();
            PrMainController main = new PrMainController();
            main.StartGame(startParams);
            PrCell[,] field = main.gameField.cells;

            utils.Assert.IsTrue(field.GetLength(0) == startParams.gameFieldSize.x);
            utils.Assert.IsTrue(field.GetLength(1) == startParams.gameFieldSize.y);
            int allCrystallsCount = PrintField(field, sb);
            utils.Assert.IsTrue(allCrystallsCount == startParams.crystalsPerPlayerNumber * startParams.playersNumber + 1);
            sb.AppendLine("===== TestGameFieldGeneration success =====");
        }
    }

}
