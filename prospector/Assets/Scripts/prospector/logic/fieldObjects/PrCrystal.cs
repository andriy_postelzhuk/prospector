﻿namespace prospector.logic.fieldObjects
{

    public class PrCrystal : PrFieldObject
    {

        public PrCrystal(int id, utils.SIntVector pos) :
            base(id, pos)
        {
        }

        public override EPrFOType typeID
        {
            get { return EPrFOType.CRYSTAL; }
        }
    }

}
