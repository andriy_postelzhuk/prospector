﻿using System.Collections.Generic;

namespace prospector.logic
{

    public class PrGameEvents
    {

        private Queue<SPrGameEvent> mEvents;

        public PrGameEvents(bool active)
        {
            if (active)
            {
                mEvents = new Queue<SPrGameEvent>();
            }
        }

        public bool isActive
        {
            get { return mEvents != null; }
        }

        public void AddChangeHPEvent(int playerID, int delta)
        {
            if (!isActive)
            {
                return;
            }
            if (delta == 0)
            {
                return;
            }
            SPrGameEvent.EType type = SPrGameEvent.EType.DAMAGED;
            if (delta > 0)
            {
                type = SPrGameEvent.EType.HEALING;
            }
            SPrGameEvent e = new SPrGameEvent(type, playerID, delta);
            mEvents.Enqueue(e);
        }

        public void AddAttackEvent(int playerID, int attackValue)
        {
            if (!isActive)
            {
                return;
            }
            mEvents.Enqueue(new SPrGameEvent(SPrGameEvent.EType.ATTACK, playerID, attackValue));
        }

        public bool hasEvents
        {
            get { return isActive && mEvents.Count > 0; }
        }

        public SPrGameEvent PullNextEvent()
        {
            return mEvents.Dequeue();
        }
    }

}
