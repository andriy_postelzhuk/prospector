﻿using System;
using System.Net;

namespace HTTPServerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StartHTTPServer("http://localhost:8080/");
        }

        // This example requires the System and System.Net namespaces.
        public static void StartHTTPServer(string url)
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }

            // Create a listener.
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            Console.WriteLine("Listening...");
            for (;;)
            {
                // Note: The GetContext method blocks while waiting for a request. 
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                System.IO.Stream input = request.InputStream;
                byte[] inputBuffer = new byte[request.ContentLength64];
                input.Read(inputBuffer, 0, (int)request.ContentLength64);
                string reqString = System.Text.Encoding.UTF8.GetString(inputBuffer);
                input.Close();
                // Obtain a response object.
                HttpListenerResponse response = context.Response;
                response.AddHeader("Access-Control-Allow-Credentials", "true");
                response.AddHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
                response.AddHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
                response.AddHeader("Access-Control-Allow-Origin", "*");
                // Construct a response.
                string responseString = ProspectorLogic.Run(reqString);
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                // You must close the output stream.
                output.Close();
            }
            
            listener.Stop();
        }
    }
}
