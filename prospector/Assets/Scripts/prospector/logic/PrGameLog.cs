﻿using System.Collections.Generic;

namespace prospector.logic
{

    public static class PrGameLog
    {
        private const int MAX_MESSAGES_COUNT = 15;

        private static Dictionary<int, List<string>> sPlayersLogs;
        private static System.Text.StringBuilder sSB;

        public static void Activate()
        {
            if (sPlayersLogs == null)
            {
                sPlayersLogs = new Dictionary<int, List<string>>();
                sSB = new System.Text.StringBuilder();
            }
            else
            {
                sPlayersLogs.Clear();
                sSB.Length = 0;
            }
        }

        public static void Deactivate()
        {
            sPlayersLogs = null;
        }

        public static void AddLogForPlayer(int playerID, string message)
        {
            if (sPlayersLogs == null || string.IsNullOrEmpty(message))
            {
                return;
            }
            #if UNITY_5
            UnityEngine.Debug.Log(string.Format("player: {0}\nmessage:\n{1}", playerID, message));
            #endif
            List<string> playerLogs;
            if (!sPlayersLogs.TryGetValue(playerID, out playerLogs))
            {
                playerLogs = new List<string>();
                sPlayersLogs.Add(playerID, playerLogs);
            }
            playerLogs.Add(message);
            if (playerLogs.Count > MAX_MESSAGES_COUNT)
            {
                playerLogs.RemoveAt(0);
            }
        }

        public static string GetLogsForPlayer(int playerID)
        {
            if (sPlayersLogs == null)
            {
                return string.Empty;
            }

            List<string> playerLogs;
            if (sPlayersLogs.TryGetValue(playerID, out playerLogs))
            {
                sSB.Length = 0;
                for (int i = playerLogs.Count - 1; i >= 0; --i)
                {
                    sSB.AppendLine("===============");
                    sSB.AppendLine(playerLogs[i]);
                }
                return sSB.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

}
