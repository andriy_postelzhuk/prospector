﻿using prospector.logic.utils;

namespace prospector.logic.data
{

    public class PrGameFieldData
    {
        public int width;
        public int height;

        public PrPlayerData[] players;
        public PrFieldObjectData[] bases;
        public PrFieldObjectData[] crystals;

        public override bool Equals(object obj)
        {
            PrGameFieldData gfData = obj as PrGameFieldData;
            if (gfData == null)
            {
                return false;
            }
            if (width != gfData.width || height != gfData.height)
            {
                return false;
            }
            return AreArrayEquals(players, gfData.players) &&
                AreArrayEquals(bases, gfData.bases) &&
                AreArrayEquals(crystals, gfData.crystals);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private static bool AreArrayEquals<T>(T[] arr1, T[] arr2)
        {
            if (arr1 == null || arr2 == null)
            {
                return arr1 == arr2;
            }
            if (arr1.Length != arr2.Length)
            {
                return false;
            }
            for (int i = 0; i < arr1.Length; ++i)
            {
                if (!arr1[i].Equals(arr2[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }

}