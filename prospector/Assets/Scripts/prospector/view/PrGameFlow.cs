﻿using UnityEngine.SceneManagement;

using prospector.logic;
using prospector.logic.utils;

namespace prospector.view
{

    public static class PrGameFlow
    {
        private const string GAME_SCENE = "GameScene";
        private const string CHOOSE_PLAYER_SCENE = "ChoosePlayerScene";

        public static void StartGame(IPrPlayer[] players)
        {
            Assert.IsTrue(SceneManager.GetActiveScene().name == CHOOSE_PLAYER_SCENE);
            sPlayers = players;
            PrGameLog.Activate();
            SceneManager.LoadScene(GAME_SCENE);
        }

        public static void RestartGame()
        {
            Assert.IsTrue(SceneManager.GetActiveScene().name == GAME_SCENE);
            Assert.IsTrue(sPlayers != null);
            for (int i = 0; i < sPlayers.Length; ++i)
            {
                sPlayers[i] = sPlayers[i].Clone(i + 1);
            }
            PrGameLog.Activate();
            SceneManager.LoadScene(GAME_SCENE);
        }

        public static void StartNewGame()
        {
            Assert.IsTrue(SceneManager.GetActiveScene().name == GAME_SCENE);
            sPlayers = null;
            PrGameLog.Deactivate();
            SceneManager.LoadScene(CHOOSE_PLAYER_SCENE);
        }

        public static IPrPlayer[] players
        {
            get { return sPlayers; }
        }

        private static IPrPlayer[] sPlayers;
    }

}
