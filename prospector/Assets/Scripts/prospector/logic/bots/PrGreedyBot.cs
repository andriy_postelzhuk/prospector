﻿using prospector.logic.fieldObjects;
using prospector.logic.actions;
using System.Collections.Generic;

namespace prospector.logic.bots
{

    public class PrGreedyBot : PrBaseBot
    {

        private const string NAME = "GreedyBot";

        public PrGreedyBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrGreedyBot(id);
        }

        protected override void GenerateImproveAction()
        {
            fieldObjects.player.EPrStat improveStat = fieldObjects.player.EPrStat.AGILITY;
            if (player.stats.agility > player.stats.health)
            {
                improveStat = fieldObjects.player.EPrStat.HEALTH;
            }
            SetReadyImproveAction(new actions.PrImproveStatAction(id, improveStat));
        }

        protected override void GenerateBattleAction()
        {
            if (player.crystalsCount > 0)
            {
                fieldObjects.PrBase baseFO = mGameField.GetPlayerBase(id);
                TryDoAMoveToPos(baseFO.position);
            }
            else
            {
                List<fieldObjects.PrCrystal> sortedCrystals = GetAllCrystalsSortedInDistance();
                if (sortedCrystals.Count > 0)
                {
                    fieldObjects.PrCrystal targetCrystal = sortedCrystals[0];
                    if (player.position == targetCrystal.position)
                    {
                        SetReadyBattleAction(new PrTakeCrystalAction(id));
                    }
                    else
                    {
                        bool success = TryDoAMoveToPos(sortedCrystals[0].position);
                        if (!success)
                        {
                            utils.Assert.IsTrue(enemy.position.ManhattanDistance(player.position) == 1);
                            SetReadyBattleAction(new PrAttackAction(id));
                        }
                    }
                }
                else
                {
                    SetReadyBattleAction(null);
                }
            }
        }
    }
}


