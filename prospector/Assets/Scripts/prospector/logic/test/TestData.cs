﻿
using System.Text;
using prospector.logic.data;
using prospector.logic.utils;
using TinyJSON;

namespace prospector.logic.test
{

    public static class TestData
    {

        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestData Run =====");

            IPrPlayer[] players = new IPrPlayer[2];
            for (int i = 0; i < players.Length; ++i)
            {
                players[i] = new bots.PrCrazyAttackBot(i + 1);
            }

            bool gameFinished = false;
            PrEmulator emulator = new PrEmulator(players, TestGameFieldGeneration.CreateSimpleStartParams());
            emulator.OnGameFinished += delegate () { gameFinished = true; };

            while (!gameFinished)
            {
                emulator.StartTick();
                PrGameFieldData gfData = emulator.gameField.GenerateData();
                string gfJson = JSON.Dump(gfData, EncodeOptions.NoTypeHints);
                PrGameFieldData gfDataFromJson;
                JSON.MakeInto(JSON.Load(gfJson), out gfDataFromJson);
                PrGameField gfFromJson = new PrGameField(gfDataFromJson);
                PrGameFieldData gfDataFromGF = gfFromJson.GenerateData();
                Assert.IsTrue(gfData.Equals(gfDataFromJson));
                Assert.IsTrue(gfDataFromGF.Equals(gfDataFromJson));
                Assert.IsTrue(gfData.Equals(gfDataFromGF));
                Assert.IsTrue(gfDataFromJson.Equals(gfDataFromGF));
            }
            PrGameFieldData gfPrintData = emulator.gameField.GenerateData();
            string gfPrintJson = JSON.Dump(gfPrintData, EncodeOptions.NoTypeHints | EncodeOptions.PrettyPrint);
            sb.AppendLine(gfPrintJson);
            utils.Assert.IsTrue(emulator.gameState == EPrGameState.FINISHED);
            sb.AppendLine("===== TestData success =====");
        }
    }
}