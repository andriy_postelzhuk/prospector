﻿
namespace prospector.logic.fieldObjects.player
{
    public enum EPrMoveDir
    {
        Undefined,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
