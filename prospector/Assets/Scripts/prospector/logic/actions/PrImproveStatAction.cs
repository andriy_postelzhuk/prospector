﻿using prospector.logic.fieldObjects.player;

namespace prospector.logic.actions
{

    public class PrImproveStatAction : PrBaseImproveStatAction
    {

        private readonly EPrStat mStat;

        public PrImproveStatAction(int playerID, EPrStat stat) :
            base(playerID)
        {
            mStat = stat;
        }

        protected override void AppendCommandInfo(PrMessageBuilder mb)
        {
            mb.AppendLine("[ImproveStat] " + mStat);
        }

        protected override bool IsCanDo(PrGameField gameField, PrMessageBuilder mb)
        {
            utils.Assert.IsTrue(IsValidPlayerID(gameField));
            if (!gameField.GetPlayerByID(playerID).stats.CanImproveStat(mStat))
            {
                mb.AppendLine("Not enough stat points");
                return false;
            }
            return true;
        }

        protected override void DoAction(PrGameField gameField, PrGameEvents events)
        {
            gameField.GetPlayerByID(playerID).stats.ImproveStat(mStat);
        }
    }

}
