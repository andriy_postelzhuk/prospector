﻿using System.Text;

namespace prospector.logic.test
{

    public static class TestSuicide
    {
        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestSuicide Run =====");

            IPrPlayer[] players = new IPrPlayer[2];
            for (int i = 0; i < players.Length; ++i)
            {
                players[i] = new bots.PrSuicideBot(i + 1);
            }
            PrEmulator emulator = new PrEmulator(players, TestGameFieldGeneration.CreateSimpleStartParams());

            for (int i = 0; i < 40; ++i)
            {
                emulator.StartTick();
            }

            utils.Assert.IsTrue(emulator.gameState == EPrGameState.FINISHED);

            sb.AppendLine("===== TestSuicide success =====");
        }
    }

}

