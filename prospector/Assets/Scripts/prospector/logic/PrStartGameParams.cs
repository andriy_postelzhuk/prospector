﻿
namespace prospector.logic
{

    public class PrStartGameParams
    {   
        public int[] playerInternalIDs;
        public int playersNumber
        {
            get { return playerInternalIDs.Length; }
        }
        public utils.SIntVector gameFieldSize;
        public int crystalsPerPlayerNumber;
        public int randomSeed;

        public int playerImproveStatPoints;

        public PrGameEvents gameEvents;

        public bool isDebug;

        public static PrStartGameParams CreateParams(int width, int height, int crystalsNumber, int seed)
        {
            PrStartGameParams startParams = new PrStartGameParams();
            if (seed == -1)
            {
                seed = (int)System.DateTime.Now.Ticks;
            }
            startParams.randomSeed = seed;
            startParams.crystalsPerPlayerNumber = crystalsNumber;
            startParams.gameFieldSize = new utils.SIntVector(width, height);
            return startParams;
        }
    }

}
