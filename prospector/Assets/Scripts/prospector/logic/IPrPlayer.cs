﻿
namespace prospector.logic
{

    public interface IPrPlayer
    {
        int id { get; }
        string pureName { get; }
        string name { get; }
        string avatarURL { get; }
        int viewSeed { get; }

        void WaitForImproveStatAction(PrGameField gameField);
        event System.Action OnImproveStatActionReady;
        actions.PrBaseImproveStatAction improveStatAction { get; }

        void WaitForBattleAction(PrGameField gameField);
        event System.Action OnBattleActionReady;
        actions.PrBattleAction battleAction { get; }

        IPrPlayer Clone(int id);
    }

}
