﻿using System;
using prospector.logic.fieldObjects;

namespace prospector.logic.bots
{

    public class PrSevereBot : PrBaseBot
    {

        private const string NAME = "SevereBot";

        public PrSevereBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrSevereBot(id);
        }

        protected override void GenerateImproveAction()
        {
            fieldObjects.player.EPrStat improveStat = fieldObjects.player.EPrStat.ATTACK;
            if (player.stats.attack == 5)
            {
                improveStat = fieldObjects.player.EPrStat.AGILITY;
            }
            if (player.stats.agility == 5)
            {
                improveStat = fieldObjects.player.EPrStat.HEALTH;
            }
            SetReadyImproveAction(new actions.PrImproveStatAction(id, improveStat));
        }

        protected override void GenerateBattleAction()
        {
            if (!IsCanKill(player, enemy, player.actionPoints))
            {
                if (enemy.getStartTurnActionPoints * enemy.stats.attack > player.healthPoints)
                {
                    PrBase baseFO = mGameField.GetPlayerBase(id);
                    bool succesMove = TryDoAMoveToPos(baseFO.position);
                    if (!succesMove)
                    {
                        SetReadyBattleAction(new actions.PrAttackAction(id));
                    }
                    return;
                }
            }

            int distance = player.position.ManhattanDistance(enemy.position);
            if (distance > PrAllConsts.ATTACK_RANGE)
            {
                bool successMove = TryDoAMoveToPos(enemy.position);
                utils.Assert.IsTrue(successMove);
            }
            else
            {
                SetReadyBattleAction(new actions.PrAttackAction(id));
            }
        }

        private bool IsCanKill(PrPlayerFO attacker, PrPlayerFO victim, int attackerActionPoints)
        {
            int distance = attacker.position.ManhattanDistance(victim.position);

            int attackActions = attackerActionPoints;
            int deltaDistance = distance - PrAllConsts.ATTACK_RANGE;
            if (deltaDistance > 0)
            {
                attackActions -= deltaDistance;
            }
            return victim.healthPoints < attackActions * attacker.stats.attack;
        }

    }
}
                