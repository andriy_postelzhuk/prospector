﻿using UnityEngine;
using UnityEngine.UI;

namespace prospector.view
{

    public class PrTextBar : MonoBehaviour
    {
        public Image backValueImg;
        public Text valueLabel;
        public Text label;
    }

}
