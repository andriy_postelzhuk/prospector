﻿using System.Text;
using prospector.logic.utils;
using prospector.logic.fieldObjects;
using prospector.logic.fieldObjects.player;

namespace prospector.logic.test
{

    public static class TestPlayer
    {
        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestPlayer Run =====");
            PrPlayerFO player = new PrPlayerFO(1, new SIntVector(0, 0), 46);
            while (player.stats.CanImproveAnySkill())
            {
                player.stats.ImproveMinSkill();
            }
            Assert.IsTrue(player.stats.agility == 5);
            Assert.IsTrue(player.stats.health == 5);
            Assert.IsTrue(player.stats.attack == 5);
            player.OnStartBattle();
            int maxHealth = 5 * PrAllConsts.HEALTH_POINTS_COEF;
            Assert.IsTrue(player.healthPoints == maxHealth);
            Assert.IsTrue(!player.isDead);
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 5);
            Assert.IsTrue(player.position == new SIntVector(0, 0));
            player.MoveAction(EPrMoveDir.DOWN);
            player.MoveAction(EPrMoveDir.RIGHT);
            player.MoveAction(EPrMoveDir.RIGHT);
            Assert.IsTrue(player.actionPoints == 2);
            player.TakeCrystalAction();
            player.AttackAction();
            Assert.IsTrue(player.actionPoints == 0);
            Assert.IsTrue(player.position == new SIntVector(2, 1));
            player.ChangeHealthPoints(-5);
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 3);
            player.MoveAction(EPrMoveDir.LEFT);
            player.MoveAction(EPrMoveDir.UP);
            player.MoveAction(EPrMoveDir.LEFT);
            Assert.IsTrue(player.actionPoints == 0);
            Assert.IsTrue(player.position == new SIntVector(0, 0));
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            player.PutCrystalls();
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 5);
            Assert.IsTrue(player.gatheredCrystals == 1);
            player.TakeCrystalAction();
            player.TakeCrystalAction();
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 2);
            player.TakeCrystalAction();
            player.TakeCrystalAction();
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            Assert.IsTrue(player.actionPoints == 0);
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 1);
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            player.PutCrystalls();
            Assert.IsTrue(player.gatheredCrystals == 5);
            player.OnStartTurn();
            Assert.IsTrue(player.actionPoints == 5);
            Assert.IsTrue(player.healthPoints == maxHealth - 5);
            Assert.IsTrue(!player.isDead);
            player.ChangeHealthPoints(-maxHealth);
            Assert.IsTrue(player.isDead);
            sb.AppendLine("===== TestPlayer success =====");
        }
    }
}
