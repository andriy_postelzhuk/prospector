﻿
namespace prospector.logic.fieldObjects
{

    public abstract class PrFieldObject
    {

        private readonly int mInternalID;

        public PrFieldObject(int internalID, utils.SIntVector pos)
        {
            mInternalID = internalID;
            position = pos;
        }

        public PrFieldObject(data.PrFieldObjectData foData)
        {
            mInternalID = foData.id;
            position = foData.position;
        }

        public data.PrFieldObjectData GenerateFOData()
        {
            data.PrFieldObjectData foData = new data.PrFieldObjectData();
            FillFOData(foData);
            return foData;
        }

        public void FillFOData(data.PrFieldObjectData foData)
        {
            foData.id = internalID;
            foData.position = position;
        }

        public int objID
        {
            get
            {
                return mInternalID * (int)EPrFOType.COUNT + (int)typeID;
            }
        }

        public utils.SIntVector position { get; protected set; }

        public int internalID
        {
            get { return mInternalID; }
        }

        public abstract EPrFOType typeID { get; }

    }

}
