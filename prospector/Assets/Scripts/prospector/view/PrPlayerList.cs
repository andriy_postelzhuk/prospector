﻿using UnityEngine;

using System.Collections.Generic;

using prospector.logic;
using prospector.logic.bots;
using prospector.logic.utils;

namespace prospector.view
{

    public class PrPlayerList
    {

        private Dictionary<string, IPrPlayer> mAllPlayers;

        public PrPlayerList()
        {
            mAllPlayers = new Dictionary<string, IPrPlayer>();
            AddPlayer(new PrCrazyAttackBot(0));
            AddPlayer(new PrSuicideBot(0));
            AddPlayer(new PrGreedyBot(0));
            AddPlayer(new PrSleepBot(0));
            AddPlayer(new PrStupidAttackBot(0));
            AddPlayer(new PrSevereBot(0));
        }

        public event System.Action<string> NewPlayerEvent;

        public event System.Action<string> RemovePlayerEvent;

        public IEnumerable<string> allPlayerNames
        {
            get
            {
                foreach (var pair in mAllPlayers)
                {
                    yield return pair.Key;
                }
            }
        }

        public void AddHTTPPlayer(string name, string serverUrl, string avatarUrl, int viewSeed)
        {
            AddPlayer(new http.PrHTTPPlayer(0, name, serverUrl, avatarUrl, viewSeed));
        }

        public void RemoveHTTPPlayer(string name)
        {
            bool removed = mAllPlayers.Remove(name);
            Assert.IsTrue(removed);
            RemovePlayerEvent.Call(name);
        }

        public IPrPlayer CreatePlayer(int id, string name)
        {
            return mAllPlayers[name].Clone(id);
        }

        private void AddPlayer(IPrPlayer newPlayer)
        {
            mAllPlayers.Add(newPlayer.pureName, newPlayer);
            NewPlayerEvent.Call(newPlayer.pureName);
        }
    }
}
