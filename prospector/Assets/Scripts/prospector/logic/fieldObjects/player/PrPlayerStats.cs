﻿
namespace prospector.logic.fieldObjects.player
{

    public class PrPlayerStats
    {
        public const int START_STAT_VALUE = 1;

        private int mStatsPoints;
        private int[] mStats;

        public PrPlayerStats(int startStatPoints)
        {
            mStatsPoints = startStatPoints;
            mStats = new int[(int)EPrStat.COUNT];
            for (int i = 0; i < mStats.Length; ++i)
            {
                mStats[i] = START_STAT_VALUE;
            }
        }

        public PrPlayerStats(data.PrPlayerStatsData statsData)
        {
            mStatsPoints = statsData.statsPoints;
            mStats = new int[(int)EPrStat.COUNT];
            mStats[(int)EPrStat.AGILITY] = statsData.agility;
            mStats[(int)EPrStat.ATTACK] = statsData.attack;
            mStats[(int)EPrStat.HEALTH] = statsData.health;
        }

        public data.PrPlayerStatsData GenerateData()
        {
            data.PrPlayerStatsData statsData = new data.PrPlayerStatsData();
            statsData.agility = agility;
            statsData.health = health;
            statsData.attack = attack;
            statsData.statsPoints = mStatsPoints;
            return statsData;
        }

        public int statsPoints
        {
            get { return mStatsPoints; }
        }

        public int agility
        {
            get { return GetStatValue(EPrStat.AGILITY); }
        }

        public int health
        {
            get { return GetStatValue(EPrStat.HEALTH); }
        }

        public int attack
        {
            get { return GetStatValue(EPrStat.ATTACK); }
        }

        public void ImproveStat(EPrStat stat)
        {
            ImproveStat((int)stat);
        }

        public bool CanImproveStat(EPrStat stat)
        {
            return CanImproveStat((int)stat);
        }

        public bool CanImproveAnySkill()
        {
            for (int i = 0; i < mStats.Length; ++i)
            {
                if (CanImproveStat(i))
                {
                    return true;
                }
            }
            return false;
        }

        public void ImproveMinSkill()
        {
            int minIndex = 0;
            int minValue = mStats[0];
            for (int i = 1; i < mStats.Length; ++i)
            {
                if (mStats[i] < minValue)
                {
                    minValue = mStats[i];
                    minIndex = i;
                }
            }
            ImproveStat(minIndex);
        }

        public int GetStatValue(EPrStat stat)
        {
            return mStats[(int)stat];
        }

        private void ImproveStat(int index)
        {
            int newValue = mStats[index] + 1;
            utils.Assert.IsTrue(CanImproveStat(index));
            mStatsPoints -= newValue;
            mStats[index] = newValue;
        }

        private bool CanImproveStat(int index)
        {
            return mStats[index] < mStatsPoints;
        }
    }
}
