﻿using UnityEngine;

namespace prospector.view.utils
{

    public static class ViewUtils
    {
        public static T AddChild<T>(this Component comp, T prefab) where T : Component
        {
            return AddChild(comp.transform, prefab);
        }

        public static T AddChild<T>(this Transform trans, T prefab) where T : Component
        {
            T child = Object.Instantiate(prefab) as T;
            child.transform.SetParent(trans);
            child.transform.localScale = Vector3.one;
            return child;
        }
    }

}
