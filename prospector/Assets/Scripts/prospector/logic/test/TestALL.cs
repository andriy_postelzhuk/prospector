﻿using System.Text;

namespace prospector.logic.test
{

    public static class TestALL
    {
        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestALL Run =====");
            TestIntVector.Run(sb);
            TestGameFieldGeneration.Run(sb);
            TestStatsImprovements.Run(sb);
            TestPlayer.Run(sb);
            TestTurns.Run(sb);
            TestSuicide.Run(sb);
            TestData.Run(sb);
            TestActivityGuard.Run(sb);
            sb.AppendLine("===== TestALL success =====");
        }
    }

}
