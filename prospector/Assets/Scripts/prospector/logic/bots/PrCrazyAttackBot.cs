﻿using System;
using prospector.logic.fieldObjects;

namespace prospector.logic.bots
{

    public class PrCrazyAttackBot : PrBaseBot
    {

        private const string NAME = "CrazyAttackBot";

        public PrCrazyAttackBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrCrazyAttackBot(id);
        }

        protected override void GenerateImproveAction()
        {
            fieldObjects.player.EPrStat improveStat = fieldObjects.player.EPrStat.ATTACK;
            if (player.stats.attack > player.stats.agility)
            {
                improveStat = fieldObjects.player.EPrStat.AGILITY;
            }
            SetReadyImproveAction(new actions.PrImproveStatAction(id, improveStat));
        }

        protected override void GenerateBattleAction()
        {
            int distance = enemy.position.ManhattanDistance(player.position);
            if (distance > PrAllConsts.ATTACK_RANGE)
            {
                bool successMove = TryDoAMoveToPos(enemy.position);
                utils.Assert.IsTrue(successMove);
            }
            else
            {
                SetReadyBattleAction(new actions.PrAttackAction(id));
            }
        }
    }
}

