﻿
namespace prospector.logic
{

    public struct SPrGameEvent
    {
        public enum EType
        {
            ATTACK,
            DAMAGED,
            HEALING
        }

        public EType type;
        public int playerID;
        public int value;

        public SPrGameEvent(EType type, int playerID, int value)
        {
            this.type = type;
            this.playerID = playerID;
            this.value = value;
        }
    }

}
