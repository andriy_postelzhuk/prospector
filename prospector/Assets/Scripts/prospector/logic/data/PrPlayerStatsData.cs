﻿
namespace prospector.logic.data
{

    public class PrPlayerStatsData
    {
        public int health;
        public int agility;
        public int attack;
        public int statsPoints;

        public override bool Equals(object obj)
        {
            PrPlayerStatsData statsData = obj as PrPlayerStatsData;
            if (statsData == null)
            {
                return false;
            }
            return health == statsData.health &&
                agility == statsData.agility &&
                attack == statsData.attack &&
                statsPoints == statsData.statsPoints;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
