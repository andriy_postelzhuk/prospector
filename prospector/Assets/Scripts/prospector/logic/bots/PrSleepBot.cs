﻿
namespace prospector.logic.bots
{

    public class PrSleepBot : PrBaseBot
    {

        private const string NAME = "SleepBot";

        public PrSleepBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrSleepBot(id);
        }

        protected override void GenerateImproveAction()
        {
            SetReadyImproveAction(null);
        }

        protected override void GenerateBattleAction()
        {
            SetReadyBattleAction(null);
        }
    }
}


