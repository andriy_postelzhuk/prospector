﻿using prospector.logic.utils;

namespace prospector.logic.data
{

    public class PrFieldObjectData
    {
        public int id;
        public SIntVector position;

        public override bool Equals(object obj)
        {
            PrFieldObjectData foData = obj as PrFieldObjectData;
            if (foData == null)
            {
                return false;
            }
            return foData.id == id && foData.position == position;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
