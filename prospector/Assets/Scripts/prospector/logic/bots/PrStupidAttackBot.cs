﻿using System;
using prospector.logic.fieldObjects;

namespace prospector.logic.bots
{

    public class PrStupidAttackBot : PrBaseBot
    {

        private const string NAME = "StupidAttackBot";

        public PrStupidAttackBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrStupidAttackBot(id);
        }

        protected override void GenerateImproveAction()
        {
            SetReadyImproveAction(new actions.PrImproveStatAction(id, fieldObjects.player.EPrStat.ATTACK));
        }

        protected override void GenerateBattleAction()
        {
            int distance = enemy.position.ManhattanDistance(player.position);
            if (distance > PrAllConsts.ATTACK_RANGE)
            {
                bool successMove = TryDoAMoveToPos(enemy.position);
                utils.Assert.IsTrue(successMove);
            }
            else
            {
                SetReadyBattleAction(new actions.PrAttackAction(id));
            }
        }
    }
}


