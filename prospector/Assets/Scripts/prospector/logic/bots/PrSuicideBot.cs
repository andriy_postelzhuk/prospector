﻿using System;
using prospector.logic.fieldObjects;

namespace prospector.logic.bots
{

    public class PrSuicideBot : PrBaseBot
    {

        private const string NAME = "SuicideBot";

        public PrSuicideBot(int id) :
            base(id, NAME)
        {
        }

        public override IPrPlayer Clone(int id)
        {
            return new PrSuicideBot(id);
        }

        protected override void GenerateImproveAction()
        {
            SetReadyImproveAction(new actions.PrImproveStatAction(id, fieldObjects.player.EPrStat.HEALTH));
        }

        protected override void GenerateBattleAction()
        {
            PrBase enemyBase = mGameField.GetEnemyPlayerBase(id);
            int distance = enemyBase.position.ManhattanDistance(player.position);
            if (distance == 0)
            {
                SetReadyBattleAction(null);
                return;
            }

            TryDoAMoveToPos(enemyBase.position);
        }
    }
}
