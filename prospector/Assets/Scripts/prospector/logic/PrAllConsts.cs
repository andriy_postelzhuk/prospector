﻿
namespace prospector.logic
{

    public static class PrAllConsts
    {
        public const float CRYSTALL_ACTION_DECREASE_COEF = 0.5f;
        public const int HEALTH_POINTS_COEF = 15;
        public const int ATTACK_RANGE = 2;
        public const int ATTACK_STRENGTH_OF_BASE = HEALTH_POINTS_COEF;
        public const int HEAL_STRENGTH_OF_BASE = ATTACK_STRENGTH_OF_BASE;

        public const int FIELD_WIDTH = 9;
        public const int FIELD_HEIGHT = 11;

        public const int STAT_POINTS = 35;
        public const int CRYSTAL_PER_PLAYER_NUMBER = 3;

        public static PrStartGameParams CreateStartParams()
        {
            PrStartGameParams startParams = PrStartGameParams.CreateParams(FIELD_WIDTH, FIELD_HEIGHT, CRYSTAL_PER_PLAYER_NUMBER, -1);
            startParams.playerImproveStatPoints = STAT_POINTS;
            return startParams;
        }
    }

}
