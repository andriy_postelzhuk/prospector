﻿using System.Text;
using prospector.logic.fieldObjects;
using prospector.logic.utils;

namespace prospector.logic.test
{

    public static class TestActivityGuard
    {

        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestActivityGuard Run =====");
            PrPlayerFO[] players = new PrPlayerFO[2];
            players[0] = new PrPlayerFO(1, new SIntVector(0, 0), 0);
            players[1] = new PrPlayerFO(2, new SIntVector(1, 1), 0);
            int winnerID;

            PrPlayerActivityGuard guard = new PrPlayerActivityGuard();
            for (int i = 0; i < PrPlayerActivityGuard.MAX_INACTIVITY_TICKS; ++i)
            {
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
            }
            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 2);

            players[0].OnStartTurn();
            players[0].TakeCrystalAction();

            for (int i = 0; i < PrPlayerActivityGuard.MAX_INACTIVITY_TICKS + 1; ++i)
            {
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
            }

            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 1);

            players[0].OnStartBattle();
            Assert.IsTrue(players[0].healthPoints > players[1].healthPoints);
            players[1].OnStartTurn();
            players[1].TakeCrystalAction();

            for (int i = 0; i < PrPlayerActivityGuard.MAX_INACTIVITY_TICKS + 1; ++i)
            {
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
            }
            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 1);
            players[1].OnStartBattle();

            for (int i = 0; i < PrPlayerActivityGuard.MAX_INACTIVITY_TICKS + 1; ++i)
            {
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
            }
            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 2);

            players[0].PutCrystalls();

            for (int i = 0; i < PrPlayerActivityGuard.MAX_INACTIVITY_TICKS + 1; ++i)
            {
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
            }

            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 1);

            for (; guard.allTicksCounter < PrPlayerActivityGuard.MAX_ALL_TICKS;)
            {
                players[1].OnStartTurn();
                players[1].TakeCrystalAction();
                winnerID = guard.OnTickFinished(players);
                Assert.IsTrue(winnerID == -1);
                Assert.IsTrue(guard.inactivityCounter == 0);
            }
            players[1].PutCrystalls();
            winnerID = guard.OnTickFinished(players);
            Assert.IsTrue(winnerID == 2);

            sb.AppendLine("===== TestActivityGuard success =====");
        }
    }
}
