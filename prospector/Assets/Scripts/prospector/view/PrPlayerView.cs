﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using prospector.view.utils;

namespace prospector.view
{

    public class PrPlayerView : PrFieldObjectView
    {

        private const float COLOR_MULT_COEF = 0.8f;

        public SVGImporter.SVGImage svgImage;
        public SVGImporter.SVGAsset[] allViews;

        public Transform crystalCont;
        public Transform crystalPrefab;

        public PrPlayerEventElement eventPrefab;

        private List<PrPlayerEventElement> mEvents;
        private List<Transform> mCrystals;

        public void Init(int viewSeed)
        {
            if (viewSeed < 0)
            {
                viewSeed = Random.Range(0, allViews.Length);
            }
            int index = viewSeed % allViews.Length;
            svgImage.vectorGraphics = allViews[index];
            mCrystals = new List<Transform>();
            mEvents = new List<PrPlayerEventElement>();
        }

        public override void SetCellParent(Transform cell)
        {
            base.SetCellParent(cell);
            mRectTrans.SetAsLastSibling();
        }

        public void OnTickFinished(logic.fieldObjects.PrPlayerFO player)
        {
            svgImage.color = Color.white;
            for (int i = mEvents.Count - 1; i >= 0; --i)
            {
                if (mEvents[i].OnTickFinished())
                {
                    Destroy(mEvents[i].gameObject);
                    mEvents.RemoveAt(i);
                }
            }

            while (player.crystalsCount > mCrystals.Count)
            {
                Transform crystal = crystalCont.AddChild(crystalPrefab);
                mCrystals.Add(crystal);
            }
            for (int i = 0; i < mCrystals.Count; ++i)
            {
                mCrystals[i].gameObject.SetActive(i < player.crystalsCount);
            }
        }

        public void OnGameEvent(logic.SPrGameEvent e)
        {
            Color eventColor = GetEventColor(e.type);
            svgImage.color = Color.Lerp(Color.white, eventColor, COLOR_MULT_COEF);

            PrPlayerEventElement eventElement = mRectTrans.parent.parent.AddChild(eventPrefab);
            mEvents.Add(eventElement);
            eventElement.transform.position = mRectTrans.position;
            eventElement.Init(e.value, eventColor);
        }

        private static Color GetEventColor(logic.SPrGameEvent.EType type)
        {
            switch (type)
            {
                case logic.SPrGameEvent.EType.ATTACK:
                    return Color.black;
                case logic.SPrGameEvent.EType.DAMAGED:
                    return Color.red;
                case logic.SPrGameEvent.EType.HEALING:
                    return Color.green;
            }
            return Color.black;
        }
    }

}
