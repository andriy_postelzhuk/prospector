﻿using prospector.logic.actions;
using prospector.logic.utils;
using prospector.logic.fieldObjects;
using prospector.logic.fieldObjects.player;

using System.Collections.Generic;

namespace prospector.logic
{

    public abstract class PrBasePlayer : IPrPlayer
    {

        private readonly int mID;
        private PrBattleAction mBattleAction;
        private PrBaseImproveStatAction mImproveAction;

        protected PrGameField mGameField;

        private string mPureName;
        private string mName;

        public PrBasePlayer(int id, string name)
        {
            mID = id;
            mPureName = name;
            mName = name + "_" + id;
        }

        public PrBattleAction battleAction
        {
            get { return mBattleAction; }
        }

        public int id
        {
            get { return mID; }
        }

        public string pureName
        {
            get { return mPureName; }
        }

        public string name
        {
            get { return mName; }
        }

        public virtual string avatarURL
        {
            get { return string.Empty; }
        }

        public virtual int viewSeed
        {
            get { return -1; }
        }

        public virtual PrBaseImproveStatAction improveStatAction
        {
            get { return mImproveAction; }
        }

        public event System.Action OnBattleActionReady;

        public event System.Action OnImproveStatActionReady;

        public void WaitForBattleAction(PrGameField gameField)
        {
            mBattleAction = null;
            mGameField = gameField;
            GenerateBattleAction();
        }

        public void WaitForImproveStatAction(PrGameField gameField)
        {
            mImproveAction = null;
            mGameField = gameField;
            if (player.stats.CanImproveAnySkill())
            {
                GenerateImproveAction();
            }
            else
            {
                SetReadyImproveAction(null);
            }
        }

        public abstract IPrPlayer Clone(int id);

        protected abstract void GenerateBattleAction();

        protected abstract void GenerateImproveAction();

        protected void SetReadyBattleAction(PrBattleAction battleAction)
        {
            mBattleAction = battleAction;
            OnBattleActionReady.Call();
        }

        protected void SetReadyImproveAction(PrBaseImproveStatAction improveAction)
        {
            mImproveAction = improveAction;
            OnImproveStatActionReady.Call();
        }

        protected PrPlayerFO player
        {
            get { return mGameField.GetPlayerByID(mID); }
        }

        protected PrPlayerFO enemy
        {
            get { return mGameField.GetEnemyPlayerFor(mID); }
        }

        protected List<PrCrystal> GetAllCrystalsSortedInDistance()
        {
            List<PrCrystal> sortedCrystals = new List<PrCrystal>(mGameField.allCrystals);
            sortedCrystals.Sort(SortCrystals);
            return sortedCrystals;
        }

        protected bool TryDoAMoveToPos(SIntVector targetPos)
        {
            return TryDoAMoveWithDelta(targetPos - player.position);
        }

        protected bool TryDoAMoveAwayFromPos(SIntVector fromPos)
        {
            return TryDoAMoveWithDelta(player.position - fromPos);
        }

        private bool TryDoAMoveWithDelta(SIntVector delta)
        {
            if (delta.ManhattanDistance() == 0)
            {
                return false;
            }

            EPrMoveDir horDir;
            EPrMoveDir verDir;
            PrMoveDirUtils.GetMoveDirs(delta, out verDir, out horDir);
            EPrMoveDir firstDir = horDir;
            EPrMoveDir secondDir = verDir;
            if (Math.Abs(delta.x) < Math.Abs(delta.y))
            {
                firstDir = verDir;
                secondDir = horDir;
            }

            if (TryDoAMove(firstDir))
            {
                return true;
            }
            if (TryDoAMove(secondDir))
            {
                return true;
            }
            return false;
        }

        private bool TryDoAMove(EPrMoveDir moveDir)
        {
            if (mGameField.CanDoAMoveAction(mID, moveDir, null))
            {
                SetReadyBattleAction(new PrMoveAction(mID, moveDir));
                return true;
            }
            return false;
        }

        private int SortCrystals(PrCrystal crystal1, PrCrystal crystal2)
        {
            int distance1 = player.position.ManhattanDistance(crystal1.position);
            int distance2 = player.position.ManhattanDistance(crystal2.position);
            return distance1.CompareTo(distance2);
        }
    }

}
