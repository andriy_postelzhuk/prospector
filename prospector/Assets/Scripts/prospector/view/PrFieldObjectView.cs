﻿using UnityEngine;

namespace prospector.view
{

    public class PrFieldObjectView : MonoBehaviour
    {

        protected RectTransform mRectTrans;
        
        protected void Awake()
        {
            mRectTrans = GetComponent<RectTransform>();
        }

        public virtual void SetCellParent(Transform cell)
        {
            if (mRectTrans.parent == cell)
            {
                return;
            }
            mRectTrans.SetParent(cell);
            mRectTrans.anchoredPosition = Vector2.zero;
            mRectTrans.localScale = Vector3.one;
        }
    }

}
