﻿using prospector.logic.fieldObjects.player;
using prospector.logic.utils;

namespace prospector.logic.fieldObjects
{

    public class PrPlayerFO : PrFieldObject
    {

        private PrPlayerStats mStats;
        private int mHealthPoints;
        private int mActionPoints;
        private int mCrystalsCount;
        private int mGatheredCrystalCount;
        private bool mIsDead;

        public PrPlayerFO(int id, SIntVector position, int startStatPoints) :
            base(id, position)
        {
            mStats = new PrPlayerStats(startStatPoints);
        }

        public PrPlayerFO(data.PrPlayerData playerData) :
            base(playerData)
        {
            mStats = new PrPlayerStats(playerData.stats);
            mHealthPoints = playerData.healthPoints;
            mActionPoints = playerData.actionsPoints;
            mGatheredCrystalCount = playerData.gatheredCrystals;
            mCrystalsCount = playerData.crystalsCount;
        }

        public event System.Action<PrPlayerFO> OnPlayerDied;

        public data.PrPlayerData GenerateData()
        {
            data.PrPlayerData playerData = new data.PrPlayerData();
            FillFOData(playerData);
            playerData.stats = stats.GenerateData();
            playerData.healthPoints = healthPoints;
            playerData.actionsPoints = actionPoints;
            playerData.gatheredCrystals = gatheredCrystals;
            playerData.crystalsCount = crystalsCount;
            return playerData;
        }

        public override EPrFOType typeID
        {
            get { return EPrFOType.PLAYER; }
        }

        public PrPlayerStats stats
        {
            get { return mStats; }
        }

        public bool isDead
        {
            get { return mIsDead; }
        }

        public int healthPoints
        {
            get { return mHealthPoints; }
        }

        public int actionPoints
        {
            get { return mActionPoints; }
        }

        public bool canDoAnAction
        {
            get { return mActionPoints > 0 && !isDead; }
        }

        public int gatheredCrystals
        {
            get { return mGatheredCrystalCount; }
        }

        public int crystalsCount
        {
            get { return mCrystalsCount; }
        }

        public int maxHealthPoints
        {
            get { return mStats.health * PrAllConsts.HEALTH_POINTS_COEF; }
        }

        public int getStartTurnActionPoints
        {
            get
            {
                int value = mStats.agility;
                for (int i = 0; i < mCrystalsCount; ++i)
                {
                    int decreasePoints = (int)(value * PrAllConsts.CRYSTALL_ACTION_DECREASE_COEF);
                    value -= decreasePoints;
                }
                return value;
            }
        }

        public void OnStartBattle()
        {
            mHealthPoints = maxHealthPoints;
        }

        public void OnStartTurn()
        {
            mActionPoints = getStartTurnActionPoints;
        }

        public int ChangeHealthPoints(int delta)
        {
            Assert.IsTrue(!mIsDead && mHealthPoints > 0);
            int healthBefore = mHealthPoints;
            mHealthPoints += delta;
            if (mHealthPoints <= 0)
            {
                mIsDead = true;
                mHealthPoints = 0;
                OnPlayerDied.Call(this);
            }
            else if (mHealthPoints > maxHealthPoints)
            {
                mHealthPoints = maxHealthPoints;
            }
            return mHealthPoints - healthBefore;
        }

        public void PutCrystalls()
        {
            if (mCrystalsCount > 0)
            {
                mGatheredCrystalCount += mCrystalsCount;
                mCrystalsCount = 0;
            }
        }

        public void MoveAction(EPrMoveDir moveDir)
        {
            DecreaseActionPoints();
            position += PrMoveDirUtils.MoveVector(moveDir);
        }

        public void AttackAction()
        {
            DecreaseActionPoints();
        }

        public void TakeCrystalAction()
        {
            DecreaseActionPoints();
            ++mCrystalsCount;
        }

        public void SleepAction()
        {
            DecreaseActionPoints();
        }

        private void DecreaseActionPoints()
        {
            Assert.IsTrue(canDoAnAction);
            --mActionPoints;
        }
    }

}

