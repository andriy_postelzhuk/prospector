﻿using prospector.logic.utils;

namespace prospector.logic.fieldObjects.player
{

    public static class PrMoveDirUtils
    {
        public static SIntVector MoveVector(EPrMoveDir move)
        {
            int x = 0;
            int y = 0;
            switch (move)
            {
                case EPrMoveDir.DOWN:
                    y = 1;
                    break;
                case EPrMoveDir.UP:
                    y = -1;
                    break;
                case EPrMoveDir.LEFT:
                    x = -1;
                    break;
                case EPrMoveDir.RIGHT:
                    x = 1;
                    break;
            }
            return new SIntVector(x, y);
        }

        public static void GetMoveDirs(SIntVector dirVec, out EPrMoveDir verticalDir, out EPrMoveDir horizontalDir)
        {
            verticalDir = EPrMoveDir.Undefined;
            horizontalDir = EPrMoveDir.Undefined;
            if (dirVec.x > 0)
            {
                horizontalDir = EPrMoveDir.RIGHT;
            }
            else if (dirVec.x < 0)
            {
                horizontalDir = EPrMoveDir.LEFT;
            }

            if (dirVec.y > 0)
            {
                verticalDir = EPrMoveDir.DOWN;
            }
            else if (dirVec.y < 0)
            {
                verticalDir = EPrMoveDir.UP;
            }
        }
    }

}
