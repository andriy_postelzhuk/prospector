﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TestHTTP : MonoBehaviour
{

    private const int MAX_LOG_MESSAGES = 5;

    public InputField urlInput;
    public InputField dataInput;
    public Text logOutput;

    private System.Text.StringBuilder mResultLog;
    private Queue<string> mMessages;
    private WWW mWWW;

    void Awake()
    {
        mMessages = new Queue<string>();
        mResultLog = new System.Text.StringBuilder();
    }

    public void OnClickSendRequest()
    {
        if (mWWW != null)
        {
            AddLogMessage("Request in progress");
        }

        StartCoroutine(SendRequest());
    }

    private IEnumerator SendRequest()
    {
        mWWW = new WWW(urlInput.text, System.Text.Encoding.UTF8.GetBytes(dataInput.text));
        float startReqTime = Time.realtimeSinceStartup;
        yield return mWWW;
        float delta = Time.realtimeSinceStartup - startReqTime;
        
        if (string.IsNullOrEmpty(mWWW.error))
        {
            AddLogMessage(string.Format("success: {0} \n response: {1}", delta, mWWW.text));
        }
        else
        {
            AddLogMessage(string.Format("error: {0} \n response: {1}", delta, mWWW.error));
        }
        mWWW.Dispose();
        mWWW = null;
    }

    private void AddLogMessage(string message)
    {
        mMessages.Enqueue(message);
        if (mMessages.Count > MAX_LOG_MESSAGES)
        {
            mMessages.Dequeue();
        }
        mResultLog.Length = 0;
        foreach (string s in mMessages)
        {
            mResultLog.AppendLine("==============");
            mResultLog.AppendLine(s);
        }
        mResultLog.AppendLine("================");
        logOutput.text = mResultLog.ToString();
    }
}
