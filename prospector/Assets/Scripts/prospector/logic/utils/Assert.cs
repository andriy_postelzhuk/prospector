﻿
namespace prospector.logic.utils
{

public static class Assert
{
    public static void IsTrue(bool value)
    {
        if (!value)
        {
            throw new System.Exception("[prospector.logic.utils.Assert] value is not true");
        }
    }
}

}
