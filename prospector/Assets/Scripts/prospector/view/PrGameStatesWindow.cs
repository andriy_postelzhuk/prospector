﻿using UnityEngine;
using UnityEngine.UI;

using prospector.logic;

namespace prospector.view
{

    public class PrGameStatesWindow : MonoBehaviour
    {
        public Text label;
        public Button restartButton;
        public Button newGameButton;
        public Button resumeButton;

        private EPrGameState mLastGameState;
        private System.Action mOnResume;

        public void Init()
        {
            InitButton(restartButton, PrLocalization.BUTTON_LABEL_RESTART, OnClickRestart);
            InitButton(newGameButton, PrLocalization.BUTTON_LABEL_NEW_GAME, OnClickNewGame);
            InitButton(resumeButton, PrLocalization.BUTTON_LABEL_RESUME, OnClickResume);

            mLastGameState = EPrGameState.Undefined;
            label.text = "";
        }

        public void OnNewGameState(EPrGameState gameState)
        {
            if (mLastGameState == gameState)
            {
                return;
            }
            mLastGameState = gameState;
            if (gameState == EPrGameState.CHOOSING_STATS)
            {
                label.text = PrLocalization.STATE_CHOOSING_STATS;
            }
            else
            {
                label.text = "";
            }
            UpdateView(false);
        }

        public void OnGameFinished(string winnerPlayerName, int winnerActionsCount)
        {
            mLastGameState = EPrGameState.FINISHED;
            string text = PrLocalization.STATE_FINISHING.Replace("[PLAYER_NAME]", winnerPlayerName);
            text = text.Replace("[ACTIONS_COUNT]", winnerActionsCount.ToString());
            label.text = text;
            UpdateView(false);
        }

        public void Pause(System.Action onResumeCallback)
        {
            mOnResume = onResumeCallback;
            UpdateView(true);
        }

        private void UpdateView(bool isPause)
        {
            gameObject.SetActive(isPause || mLastGameState != EPrGameState.BATTLE);
            resumeButton.gameObject.SetActive(isPause);
            restartButton.gameObject.SetActive(isPause || mLastGameState == EPrGameState.FINISHED);
            newGameButton.gameObject.SetActive(isPause || mLastGameState == EPrGameState.FINISHED);
        }

        private static void InitButton(Button but, string labelText, UnityEngine.Events.UnityAction action)
        {
            but.GetComponentInChildren<Text>().text = labelText;
            but.onClick.AddListener(action);
            but.gameObject.SetActive(false);
        }

        private void OnClickRestart()
        {
            PrGameFlow.RestartGame();
        }

        private void OnClickNewGame()
        {
            PrGameFlow.StartNewGame();
        }

        private void OnClickResume()
        {
            mOnResume();
            mOnResume = null;
            UpdateView(false);
        }
    }

}
