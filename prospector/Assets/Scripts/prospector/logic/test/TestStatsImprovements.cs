﻿using System.Text;
using prospector.logic.fieldObjects.player;

namespace prospector.logic.test
{

    public static class TestStatsImprovements
    {
        public static void Run(StringBuilder sb)
        {
            sb.AppendLine("===== TestStatsImprovements Run =====");
            PrStartGameParams startParams = TestGameFieldGeneration.CreateSimpleStartParams();
            startParams.playerImproveStatPoints = 7;
            PrMainController main = new PrMainController();
            main.StartGame(startParams);

            actions.PrImproveStatAction[] arr = new actions.PrImproveStatAction[2];

            utils.Assert.IsTrue(main.curState == EPrGameState.CHOOSING_STATS);

            arr[0] = new actions.PrImproveStatAction(1, EPrStat.AGILITY);
            arr[1] = new actions.PrImproveStatAction(2, EPrStat.ATTACK);
            main.ChooseStatsTurn(arr);

            utils.Assert.IsTrue(main.curState == EPrGameState.CHOOSING_STATS);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.CanImproveAnySkill());
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.CanImproveAnySkill());
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.CanImproveStat(EPrStat.AGILITY));
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.CanImproveStat(EPrStat.AGILITY));
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.CanImproveStat(EPrStat.ATTACK));
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.CanImproveStat(EPrStat.ATTACK));

            arr[0] = new actions.PrImproveStatAction(1, EPrStat.HEALTH);
            arr[1] = new actions.PrImproveStatAction(2, EPrStat.ATTACK);
            main.ChooseStatsTurn(arr);

            utils.Assert.IsTrue(main.curState == EPrGameState.CHOOSING_STATS);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.CanImproveStat(EPrStat.AGILITY));
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.CanImproveStat(EPrStat.AGILITY));
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.CanImproveStat(EPrStat.ATTACK));
            utils.Assert.IsTrue(!main.gameField.GetPlayerByID(2).stats.CanImproveStat(EPrStat.ATTACK));

            arr[0] = new actions.PrImproveStatAction(1, EPrStat.ATTACK);
            arr[1] = new actions.PrImproveStatAction(2, EPrStat.ATTACK);
            main.ChooseStatsTurn(arr);

            utils.Assert.IsTrue(main.curState == EPrGameState.BATTLE);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.agility == 2);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.attack == 2);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(1).stats.health == 2);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.agility == 1);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.attack == 3);
            utils.Assert.IsTrue(main.gameField.GetPlayerByID(2).stats.health == 2);
            sb.AppendLine("===== TestStatsImprovements success =====");
        }
    }

}
