﻿using prospector.logic.utils;

namespace prospector.logic
{

    public class PrEmulator
    {

        private readonly IPrPlayer[] mPlayers;
        private readonly PrMainController mMainController;

        private int mWaitPlayerEventCounter;

        public PrEmulator(IPrPlayer[] players, PrStartGameParams startParams)
        {
            mPlayers = players;
            startParams.playerInternalIDs = new int[mPlayers.Length];
            for (int i = 0; i < mPlayers.Length; ++i)
            {
                mPlayers[i].OnBattleActionReady += OnBattleActionReady;
                mPlayers[i].OnImproveStatActionReady += OnImproveStatActionReady;
                startParams.playerInternalIDs[i] = mPlayers[i].id;
            }
            mMainController = new PrMainController();
            mMainController.StartGame(startParams);
        }

        public PrGameField gameField
        {
            get { return mMainController.gameField; }
        }

        public EPrGameState gameState
        {
            get { return mMainController.curState; }
        }

        public string winnerPlayerName
        {
            get
            {
                foreach (var player in mPlayers)
                {
                    if (player.id == mMainController.winnerID)
                    {
                        return player.name;
                    }
                }
                return null;
            }
        }

        public int winnerActionCount
        {
            get { return mMainController.winnerActionCount; }
        }

        public void StartTick()
        {
            Assert.IsTrue(mWaitPlayerEventCounter == 0);
            switch (mMainController.curState)
            {
                case EPrGameState.CHOOSING_STATS:
                    mWaitPlayerEventCounter = mPlayers.Length;
                    foreach (IPrPlayer player in mPlayers)
                    {
                        player.WaitForImproveStatAction(mMainController.gameField);
                    }
                break;
                case EPrGameState.BATTLE:
                    mWaitPlayerEventCounter = 1;
                    waitPlayer.WaitForBattleAction(mMainController.gameField);
                break;
                case EPrGameState.FINISHED:
                    OnGameFinished.Call();
                break;
                default:
                    Assert.IsTrue(false);
                break;
            }
        }

        public event System.Action OnTickFinished;

        public event System.Action OnGameFinished;

        private void OnImproveStatActionReady()
        {
            Assert.IsTrue(mMainController.curState == EPrGameState.CHOOSING_STATS);
            --mWaitPlayerEventCounter;
            if (mWaitPlayerEventCounter > 0)
            {
                return;
            }

            actions.PrBaseImproveStatAction[] improveActions = new actions.PrBaseImproveStatAction[mPlayers.Length];
            for (int i = 0; i < mPlayers.Length; ++i)
            {
                improveActions[i] = mPlayers[i].improveStatAction;
            }
            mMainController.ChooseStatsTurn(improveActions);
            OnTickFinished.Call();
        }

        private void OnBattleActionReady()
        {
            Assert.IsTrue(mMainController.curState == EPrGameState.BATTLE);
            --mWaitPlayerEventCounter;
            Assert.IsTrue(mWaitPlayerEventCounter == 0);
            mMainController.BattleTurn(waitPlayer.battleAction);
            OnTickFinished.Call();
        }

        private IPrPlayer waitPlayer
        {
            get { return GetPlayerByID(mMainController.activePlayerID); }
        }

        private IPrPlayer GetPlayerByID(int id)
        {
            foreach (var player in mPlayers)
            {
                if (player.id == id)
                {
                    return player;
                }
            }
            return null;
        }
    }

}
